package com.sber.spring.SpringProject2.filmoteka.service;

import com.sber.spring.SpringProject2.filmoteka.dto.AddDirectorDto;
import com.sber.spring.SpringProject2.filmoteka.dto.FilmDto;
import com.sber.spring.SpringProject2.filmoteka.dto.FilmWithDirectorsDto;
import com.sber.spring.SpringProject2.filmoteka.exception.NotFoundException;
import com.sber.spring.SpringProject2.filmoteka.mapper.FilmMapper;
import com.sber.spring.SpringProject2.filmoteka.mapper.FilmWithDirectorsMapper;
import com.sber.spring.SpringProject2.filmoteka.model.Film;
import com.sber.spring.SpringProject2.filmoteka.model.Genre;
import com.sber.spring.SpringProject2.filmoteka.repository.FilmRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

//Тут были CRUD операции, теперь они в абстрактном сервисе
@Service
public class FilmService extends GenericService<Film, FilmDto> {
    private final FilmWithDirectorsMapper filmWithDirectorsMapper;

    public FilmService(FilmRepository repository, FilmMapper mapper, FilmWithDirectorsMapper filmWithDirectorsMapper) {
        super(repository, mapper);
        this.filmWithDirectorsMapper = filmWithDirectorsMapper;
    }

    public FilmDto addDirector(final AddDirectorDto addDirectorDto) throws NotFoundException {
        FilmDto filmDto = getOne(addDirectorDto.getDirectorId());
        filmDto.getDirectorIds().add(addDirectorDto.getFilmId());
        update(filmDto, addDirectorDto.getDirectorId());
        return filmDto;
    }

    public List<FilmWithDirectorsDto> searchFilms(final String title) {
        List<Film> foundFilms = ((FilmRepository) repository).findAllByTitleContainsIgnoreCase(title);
        return filmWithDirectorsMapper.toDtos(foundFilms);
    }

    public List<FilmWithDirectorsDto> searchFilms(Genre genre) {
        List<Film> foundFilms = ((FilmRepository) repository).findAllByGenre(genre);
        return filmWithDirectorsMapper.toDtos(foundFilms);
    }

    public List<FilmWithDirectorsDto> listAllBooksWithDirectors() {
        return filmWithDirectorsMapper.toDtos(repository.findAll());
    }

    public FilmWithDirectorsDto getBookWithDirectorDto(final Integer bookId) throws NotFoundException {
        Film film = repository.findById(bookId).orElseThrow(() -> new NotFoundException("Film not found"));
        return filmWithDirectorsMapper.toDto(film);
    }

//    public Page<FilmWithDirectorsDto> findFilms(final FilmSearchDto filmSearchDto,
//                                                final PageRequest pageRequest) {
//        String genre = filmSearchDto.getGenre() != null ? String.valueOf(filmSearchDto.getGenre().ordinal()) : null;
//        List<Film> filmsPaginated = ((FilmRepository) repository).searchFilms(genre, filmSearchDto.getTitle());
//        List<FilmWithDirectorsDto> result = filmWithDirectorsMapper.toDtos(filmsPaginated.getContent());
//        return new PageImpl<>(result, pageRequest, filmsPaginated.getTotalElements());
//    }

    public Page<FilmWithDirectorsDto> getAllNotDeletedFilmsWithAuthors(PageRequest pageRequest) {
        Page<Film> filmsPaginated = repository.findAllByIsDeletedFalse(pageRequest);
        List<FilmWithDirectorsDto> result = filmWithDirectorsMapper.toDtos(filmsPaginated.getContent());
        return new PageImpl<>(result, pageRequest, filmsPaginated.getTotalElements());
    }
}
