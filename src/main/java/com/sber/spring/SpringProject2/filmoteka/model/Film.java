package com.sber.spring.SpringProject2.filmoteka.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.Date;
import java.util.List;

@Entity
@Table(name = "films")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Film extends GenericModel {


    @Column(name = "title", nullable = false) //поле не может быть null
    private String title;
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Genre genre;
    @Column(name = "country", nullable = false)
    private String country;
    @Column(name = "premier_year", nullable = false)
    private String premierYear;
    //столбец содержит пути до файлов
    @Column(name = "file_directory")
    private String fileDirectory;
    //@Transient поле не относится к базе данных - один из способов указать как правильно построить связь M to M
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "film_directors",
            joinColumns = @JoinColumn(name = "film_id"), foreignKey = @ForeignKey(name = "FK_FILMS_DIRECTORS"),
            inverseJoinColumns = @JoinColumn(name = "director_id"), inverseForeignKey = @ForeignKey(name = "FK_DIRECTORS_FILMS"))
    @JsonIgnore
    private List<Director> directors;

    @JsonIgnore
    @OneToMany(mappedBy = "film", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<Order> orders;
}
