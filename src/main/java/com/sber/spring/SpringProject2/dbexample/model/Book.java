package com.sber.spring.SpringProject2.dbexample.model;

import lombok.*;

import java.util.Date;
//JDBC - Java Database Connection
//DAO - Data Access Object - объект, управляющий уровнем доступа к БД
//POJO - Plain Old Java Object
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
public class Book {
    //@Setter(AccessLevel.NONE) - управление уровнем доступа к методу Set
    //@Getter(AccessLevel.NONE)
    private Integer id;
    private String title;
    private String author;
    private Date dateAdded;
}
