//package com.sber.spring.SpringProject2.filmoteka.service;
//
//import com.sber.spring.SpringProject2.filmoteka.dto.UserDto;
//import com.sber.spring.SpringProject2.filmoteka.exception.NotFoundException;
//import com.sber.spring.SpringProject2.filmoteka.mapper.UserMapper;
//import com.sber.spring.SpringProject2.filmoteka.model.User;
//import com.sber.spring.SpringProject2.filmoteka.repository.UserRepository;
//import com.sber.spring.SpringProject2.filmoteka.service.data.UserTestData;
//import lombok.extern.slf4j.Slf4j;
//import org.junit.jupiter.api.Order;
//import org.junit.jupiter.api.Test;
//import org.mockito.Mockito;
//
//import java.util.List;
//import java.util.Optional;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNotEquals;
//import static org.mockito.ArgumentMatchers.eq;
//import static org.mockito.Mockito.times;
//import static org.mockito.Mockito.verify;
//
//@Slf4j
//public class UserServiceTest extends GenericTest<User, UserDto> {
//    public UserServiceTest() {
//        repository = Mockito.mock(UserRepository.class);
//        mapper = Mockito.mock(UserMapper.class);
//        service = new UserService((UserRepository) repository, (UserMapper) mapper);
//    }
//
//    @Test
//    @Order(3)
//    @Override
//    protected void getAll() {
//        Mockito.when(repository.findAll()).thenReturn(UserTestData.USER_LIST);
//        Mockito.when(mapper.toDtos(UserTestData.USER_LIST)).thenReturn(UserTestData.USER_DTO_LIST);
//        List<UserDto> userDtos = service.listAll();
//        log.info("Testing getAll(): " + userDtos.toString());
//        assertEquals(userDtos.size(), UserTestData.USER_LIST.size());
//    }
//
//    @Test
//    @Order(2)
//    @Override
//    protected void getOne() throws NotFoundException {
//        User user1 = UserTestData.USER_1;
//        user1.setId(1);
//        UserDto userDtoInitial1 = UserTestData.USER_DTO_1;
//        userDtoInitial1.setId(1);
//
//        User user2 = UserTestData.USER_2;
//        user1.setId(2);
//        UserDto userDtoInitial2 = UserTestData.USER_DTO_2;
//        userDtoInitial1.setId(2);
//
//        Mockito.when(repository.findById(1)).thenReturn(Optional.of(user1));
//        Mockito.when(repository.findById(2)).thenReturn(Optional.of(user2));
//        Mockito.when(mapper.toDto(user1)).thenReturn(userDtoInitial1);
//        Mockito.when(mapper.toDto(user2)).thenReturn(userDtoInitial2);
//
//        UserDto userDto1 = service.getOne(1);
//        UserDto userDto2 = service.getOne(2);
//        log.info("Testing getOne(): " + userDto1);
//        log.info("Testing getOne(): " + userDto2);
//        assertEquals(userDto1, UserTestData.USER_DTO_1);
//        assertNotEquals(userDto2, UserTestData.USER_DTO_1);
//    }
//
//    @Test
//    @Order(1)
//    @Override
//    protected void create() {
//        Mockito.when(mapper.toEntity(UserTestData.USER_DTO_1)).thenReturn(UserTestData.USER_1);
//        Mockito.when(mapper.toDto(UserTestData.USER_1)).thenReturn(UserTestData.USER_DTO_1);
//        Mockito.when(repository.save(UserTestData.USER_1)).thenReturn(UserTestData.USER_1);
//        UserDto userDto = service.create(UserTestData.USER_DTO_1);
//        log.info("Testing create(): " + userDto);
//        assertEquals(userDto, UserTestData.USER_DTO_1);
//    }
//
//    @Test
//    @Order(4)
//    @Override
//    protected void update() {
//        Mockito.when(mapper.toEntity(UserTestData.USER_DTO_1)).thenReturn(UserTestData.USER_1);
//        Mockito.when(mapper.toDto(UserTestData.USER_1)).thenReturn(UserTestData.USER_DTO_1);
//        Mockito.when(repository.save(UserTestData.USER_1)).thenReturn(UserTestData.USER_1);
//        UserDto userDto = service.update(UserTestData.USER_DTO_1, 1);
//        log.info("Testing update(): " + userDto);
//        assertEquals(userDto, UserTestData.USER_DTO_1);
//    }
//
//    @Test
//    @Order(5)
//    @Override
//    protected void delete() {
//        Mockito.doNothing().when(repository).deleteById(1);
//        service.delete(1);
//        verify(repository, times(1)).deleteById(eq(1));
//    }
//
//    @Test
//    @Order(6)
//    protected void getOrdersIdsTest() {
//        User user1 = UserTestData.USER_1;
//        user1.setId(1);
//        UserDto userDtoInitial1 = UserTestData.USER_DTO_1;
//        userDtoInitial1.setId(1);
//
//        Mockito.when(repository.findById(1)).thenReturn(Optional.of(user1));
//        Mockito.when(mapper.toDto(user1)).thenReturn(userDtoInitial1);
//
//        List<Integer> userOrders = ((UserService) service).getOrders(1);
//        log.info("Testing getOrdersIds(): " + userOrders.toString());
//        assertEquals(userOrders.size(), UserTestData.USER_DTO_1.getOrderIds().size());
//    }
//
//    @Test
//    @Order(7)
//    protected void getUsersFilmsTest() {
//        User user1 = UserTestData.USER_1;
//        user1.setId(1);
//        UserDto userDtoInitial1 = UserTestData.USER_DTO_1;
//        userDtoInitial1.setId(1);
//
//        Mockito.when(repository.findById(1)).thenReturn(Optional.of(user1));
//        Mockito.when(mapper.toDto(user1)).thenReturn(userDtoInitial1);
//        Mockito.when(mapper.toEntity(userDtoInitial1)).thenReturn(user1);
//
//        List<String> userFilms = ((UserService) service).getUsersFilms(1);
//        log.info("Testing getOrdersIds(): " + userFilms.toString());
//        assertEquals(userFilms.size(), UserTestData.USER_1.getOrders().size());
//    }
//}
