//package com.sber.spring.SpringProject2.filmoteka.controller.mvc;
//
//import com.sber.spring.SpringProject2.filmoteka.service.FilmService;
//import io.swagger.v3.oas.annotations.Hidden;
//import lombok.RequiredArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//
//@Controller
//@Slf4j
//@Hidden
//@RequestMapping("/rent")
//@RequiredArgsConstructor
//public class MvcRentFilmController {
//    private final BookRentService rentService;
//    private final FilmService filmService;
//
//
//    @GetMapping("/film/{filmId}")
//    public String rentBook(@PathVariable Long bookId,
//                           Model model) throws NotFoundException {
//        model.addAttribute("book", bookService.getOne(bookId));
//        return "userBooks/rentBook";
//    }
//
//    @PostMapping("/book")
//    public String rentBook(@ModelAttribute("rentBookInfo") UserBookRentDto userBookRentDto) throws NotFoundException {
//        log.info(userBookRentDto.toString());
//        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        userBookRentDto.setUserId(customUserDetails.getId());
//        rentService.rentABook(userBookRentDto);
//        return "redirect:/rent/user-books/" + customUserDetails.getId();
//    }
//
//    @GetMapping("/return-book/{id}")
//    public String returnBook(@PathVariable Long id) {
//        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        rentService.returnBook(id);
//        return "redirect:/rent/user-books/" + customUserDetails.getId();
//    }
//
//    @GetMapping("/user-books/{id}")
//    public String userBooks(@RequestParam(value = "page", defaultValue = "1") int page,
//                            @RequestParam(value = "size", defaultValue = "5") int pageSize,
//                            @PathVariable Long id,
//                            Model model) {
//        PageRequest pageRequest = PageRequest.of(page - 1, pageSize);
//        Page<UserBookRentDto> rentInfoDTOPage = rentService.listUserRentBooks(id, pageRequest);
//        model.addAttribute("rentBooks", rentInfoDTOPage);
//        model.addAttribute("userId", id);
//        return "userBooks/viewAllUserBooks";
//    }
//
//}
