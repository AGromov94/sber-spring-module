package com.sber.spring.SpringProject2.filmoteka.mapper;

import com.sber.spring.SpringProject2.filmoteka.dto.OrderDto;
import com.sber.spring.SpringProject2.filmoteka.dto.UserDto;
import com.sber.spring.SpringProject2.filmoteka.model.Film;
import com.sber.spring.SpringProject2.filmoteka.model.Order;
import com.sber.spring.SpringProject2.filmoteka.model.User;
import com.sber.spring.SpringProject2.filmoteka.repository.FilmRepository;
import com.sber.spring.SpringProject2.filmoteka.repository.OrderRepository;
import com.sber.spring.SpringProject2.filmoteka.repository.UserRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Component
public class OrderMapper extends GenericMapperImpl<Order, OrderDto> {
    private final FilmRepository filmRepository;
    private final UserRepository userRepository;

    public OrderMapper(ModelMapper modelMapper, FilmRepository filmRepository, UserRepository userRepository) {
        super(Order.class, OrderDto.class, modelMapper);
        this.filmRepository = filmRepository;
        this.userRepository = userRepository;
    }

    @PostConstruct
    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Order.class, OrderDto.class)
                .addMappings(m -> m.skip(OrderDto::setUserId)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(OrderDto::setFilmId)).setPostConverter(toDtoConverter());

        modelMapper.createTypeMap(OrderDto.class, Order.class)
                .addMappings(m -> m.skip(Order::setUser)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Order::setFilm)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(Order source, OrderDto destination) {
        destination.setFilmId(fillFilmId(source));
        destination.setUserId(fillUserId(source));
    }

    @Override
    protected void mapSpecificFields(OrderDto source, Order destination) {
        if (!Objects.isNull(source.getFilmId())) {
            destination.setFilm(filmRepository.findById(source.getFilmId()).get());
        } else {
            destination.setFilm(null);
        }
        if (!Objects.isNull(source.getUserId())) {
            destination.setUser(userRepository.findById(source.getUserId()).get());
        } else {
            destination.setUser(null);
        }
    }

    @Override
    protected List<Integer> fillIds(Order source) {
        return null;
    }

    protected Integer fillFilmId(Order source) {
        return Objects.isNull(source) ? null : source.getFilm().getId();
    }

    protected Integer fillUserId(Order source) {
        return Objects.isNull(source) ? null : source.getUser().getId();
    }
}
