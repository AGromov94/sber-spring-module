package com.sber.spring.SpringProject2.filmoteka.mapper;

import com.sber.spring.SpringProject2.filmoteka.dto.GenericDto;
import com.sber.spring.SpringProject2.filmoteka.model.GenericModel;

import java.util.List;

/**
 * Данный интерфейс имеет основной набор методов для преобразования сущности в DTO и наоборот
 * @param <E> - Entity
 * @param <D> - DTO
 */
public interface Mapper<E extends GenericModel, D extends GenericDto> {
    E toEntity(D dto);

    D toDto(E entity);

    List<E> toEntities(List<D> dtoList);

    List<D> toDtos(List<E> entityList);
}
