package com.sber.spring.SpringProject2.dbexample.dao.impl;

import com.sber.spring.SpringProject2.dbexample.dao.IDaoLayer;
import com.sber.spring.SpringProject2.dbexample.model.User;

import java.util.List;

public class UserDaoJDBCImpl implements IDaoLayer<User, Long> {
    @Override
    public User getOneObjectById(Long aLong) {
        return null;
    }

    @Override
    public List<User> getAllObjects() {
        return null;
    }

    @Override
    public User create(User newObject) {
        return null;
    }

    @Override
    public User update(User updatedObject, Long aLong) {
        return null;
    }

    @Override
    public void deleteObjectById(Long aLong) {

    }
}
