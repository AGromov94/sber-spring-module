package com.sber.spring.SpringProject2.filmoteka.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
public class AddDirectorDto {
    Integer directorId;
    Integer filmId;
}
