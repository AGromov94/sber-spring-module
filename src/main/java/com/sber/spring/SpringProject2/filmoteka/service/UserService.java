package com.sber.spring.SpringProject2.filmoteka.service;

import com.sber.spring.SpringProject2.filmoteka.dto.RoleDto;
import com.sber.spring.SpringProject2.filmoteka.dto.UserDto;
import com.sber.spring.SpringProject2.filmoteka.mapper.UserMapper;
import com.sber.spring.SpringProject2.filmoteka.model.User;
import com.sber.spring.SpringProject2.filmoteka.repository.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class UserService extends GenericService<User, UserDto> {
    @Value("${spring.security.user.name}")
    private String adminUser;
    private final BCryptPasswordEncoder passwordEncoder;

    protected UserService(UserRepository repository, UserMapper mapper, BCryptPasswordEncoder passwordEncoder) {
        super(repository, mapper);
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDto create(UserDto userDto) {
        RoleDto roleDto = new RoleDto();
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        if (adminUser.equals(userName)) {
            roleDto.setId(2); //роль фильмотекаря
        } else {
            roleDto.setId(1); //роль пользователя
        }
        userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));
        userDto.setRole(roleDto);
        userDto.setCreatedWhen(LocalDateTime.now());
        userDto.setCreatedBy("");
        return mapper.toDto(repository.save(mapper.toEntity(userDto)));
    }

    public List<Integer> getOrders(Integer id) {
        UserDto user = super.getOne(id);
        return user.getOrderIds();
    }

    public List<String> getUsersFilms(Integer id) {
        User user = mapper.toEntity(super.getOne(id));
        List<String> films = new ArrayList<>();
        for (int i = 0; i < user.getOrders().size(); i++) {
            films.add(user.getOrders().get(i).getFilm().getTitle());
        }
        return films;
    }

    public boolean checkPassword(final String passwordToCheck,
                                 final UserDetails userDetails) {
        return passwordEncoder.matches(passwordToCheck, userDetails.getPassword());
    }

    public UserDto getUserByLogin(final String login) {
        return mapper.toDto(((UserRepository) repository).findUserByLoginAndDeletedFalse(login));
    }

    public UserDto getUserByEmail(final String email) {
        return mapper.toDto(((UserRepository) repository).findUserByEmailAndDeletedFalse(email));
    }

    public UserDto getUserByPhone(final String phone) {
        return mapper.toDto(((UserRepository) repository).findUserByPhoneAndDeletedFalse(phone));
    }

    public void sendChangePasswordEmail(final UserDto userDTO) {
        UUID uuid = UUID.randomUUID();
        userDTO.setChangePasswordToken(uuid.toString());
        update(userDTO, userDTO.getId());
    }

    public void changePassword(final String uuid,
                               final String password) {
        UserDto userDTO = mapper.toDto(((UserRepository) repository).findUserByChangePasswordToken(uuid));
        userDTO.setChangePasswordToken(null);
        userDTO.setPassword(passwordEncoder.encode(password));
        update(userDTO, userDTO.getId());
    }
}
