package com.sber.spring.SpringProject2.filmoteka.controller.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sber.spring.SpringProject2.filmoteka.dto.FilmDto;
import com.sber.spring.SpringProject2.filmoteka.model.Genre;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@RequiredArgsConstructor
public class FilmRestControllerTest extends GenericControllerTest {

    @Test
    @Order(3)
    @Override
    protected void createObject() throws Exception {
        FilmDto addedFilm = new FilmDto("Название1", Genre.DRAMA, "Страна1", "2000", new ArrayList<Integer>() {
        }, new ArrayList<Integer>() {
        });
        log.info("Testing createObject films via REST started");
        String result = mvc.perform(
                        MockMvcRequestBuilders.post("/rest/films/addFilm")
                                .content(super.asJsonString(addedFilm))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful()) //проверяем что статус ответа 200
                .andExpect(jsonPath("$.*", hasSize(greaterThan(0)))) //проверяем размер полученного JSONа
                .andReturn()
                .getResponse()
                .getContentAsString();
        assertEquals("Название1", objectMapper.readValue(result, new TypeReference<FilmDto>() {
        }).getTitle());
        log.info(addedFilm.toString());
        log.info("Testing createObject films via REST finished");
    }

    @Test
    @Order(4)
    @Override
    protected void updateObject() throws Exception {
        FilmDto updatedFilm = new FilmDto("XXX", Genre.DRAMA, "USA", "2002", new ArrayList<Integer>() {
        }, new ArrayList<Integer>() {
        });
        log.info("Testing updateObject films via REST started");
        String result = mvc.perform(
                        MockMvcRequestBuilders.put("/rest/films/updateFilm")
                                .content(super.asJsonString(updatedFilm))
                                .param("id", "5")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful()) //проверяем что статус ответа 200
                .andExpect(jsonPath("$.*", hasSize(greaterThan(0)))) //проверяем размер полученного JSONа
                .andReturn()
                .getResponse()
                .getContentAsString();
        assertEquals("XXX", objectMapper.readValue(result, new TypeReference<FilmDto>() {
        }).getTitle());
        log.info(objectMapper.readValue(result, new TypeReference<FilmDto>() {
        }).toString());
        log.info("Testing updateObject films via REST finished");

    }

    @Test
    @Order(5)
    @Override
    protected void deleteObject() throws Exception {
        log.info("Testing deleteObject films via REST started");
        String result1 = mvc.perform(
                        MockMvcRequestBuilders.get("/rest/films/getListOfFilms")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse()
                .getContentAsString();
        super.mvc.perform(MockMvcRequestBuilders
                        .delete("/rest/films/deleteFilm/6")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        String result2 = mvc.perform(
                        MockMvcRequestBuilders.get("/rest/films/getListOfFilms")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse()
                .getContentAsString();
        assertFalse(result1.length() == result2.length()); //до удаления != после удаления
        log.info("Testing deleteObject films via REST finished");
    }

    @Test
    @Order(1)
    @Override
    protected void listAll() throws Exception {
        log.info("Testing listAll films via REST started");
        String result = mvc.perform(
                        MockMvcRequestBuilders.get("/rest/films/getListOfFilms")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful()) //проверяем что статус ответа 200
                .andExpect(jsonPath("$.*", hasSize(greaterThan(0)))) //проверяем размер полученного JSONа
                .andReturn()
                .getResponse()
                .getContentAsString();
        List<FilmDto> filmDtos = objectMapper.readValue(result, new TypeReference<List<FilmDto>>() {
        });
        assertFalse(filmDtos.isEmpty()); //проверяем что список фильмов не пустой
        for (FilmDto f : filmDtos) {
            log.info("Film: " + f.toString());
        }
        log.info("Testing listAll films via REST finished");
    }

    @Test
    @Order(2)
    @Override
    protected void listOne() throws Exception {
        log.info("Testing listOne filmss via REST started");
        String result = mvc.perform(
                        MockMvcRequestBuilders.get("/rest/films/getFilm")
                                .param("id", "1")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful()) //проверяем что статус ответа 200
                .andExpect(jsonPath("$.*", hasSize(greaterThan(0)))) //проверяем размер полученного JSONа
                .andReturn()
                .getResponse()
                .getContentAsString();
        FilmDto filmDto = objectMapper.readValue(result, new TypeReference<FilmDto>() {
        });
        assertEquals("Crew", filmDto.getTitle());
        log.info(filmDto.toString());
        log.info("Testing listOne films via REST finished");
    }
}
