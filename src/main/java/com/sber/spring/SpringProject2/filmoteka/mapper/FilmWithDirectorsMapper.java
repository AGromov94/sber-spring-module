package com.sber.spring.SpringProject2.filmoteka.mapper;

import com.sber.spring.SpringProject2.filmoteka.dto.FilmWithDirectorsDto;
import com.sber.spring.SpringProject2.filmoteka.model.Film;
import com.sber.spring.SpringProject2.filmoteka.model.GenericModel;
import com.sber.spring.SpringProject2.filmoteka.repository.DirectorRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class FilmWithDirectorsMapper extends GenericMapperImpl<Film, FilmWithDirectorsDto> {

    private final DirectorRepository directorRepository;

    public FilmWithDirectorsMapper(ModelMapper modelMapper, DirectorRepository directorRepository) {
        super(Film.class, FilmWithDirectorsDto.class, modelMapper);
        this.directorRepository = directorRepository;
    }

    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Film.class, FilmWithDirectorsDto.class)
                .addMappings(m -> m.skip(FilmWithDirectorsDto::setDirectorIds)).setPostConverter(toDtoConverter());

        modelMapper.createTypeMap(FilmWithDirectorsDto.class, Film.class)
                .addMappings(m -> m.skip(Film::setDirectors)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(Film source, FilmWithDirectorsDto destination) {
        destination.setDirectorIds(fillIds(source));
    }

    @Override
    protected void mapSpecificFields(FilmWithDirectorsDto source, Film destination) {
        destination.setDirectors(directorRepository.findAllById(source.getDirectorIds()));
    }

    @Override
    protected List<Integer> fillIds(Film source) {
        return Objects.isNull(source) || Objects.isNull(source.getId())
                ? null
                : source.getDirectors().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toList());
    }
}
