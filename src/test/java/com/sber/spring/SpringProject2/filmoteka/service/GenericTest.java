package com.sber.spring.SpringProject2.filmoteka.service;

import com.sber.spring.SpringProject2.filmoteka.dto.GenericDto;
import com.sber.spring.SpringProject2.filmoteka.exception.NotFoundException;
import com.sber.spring.SpringProject2.filmoteka.mapper.GenericMapperImpl;
import com.sber.spring.SpringProject2.filmoteka.model.GenericModel;
import com.sber.spring.SpringProject2.filmoteka.repository.GenericRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

//ER - expected result
//AR - actual result
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public abstract class GenericTest<E extends GenericModel, D extends GenericDto> {
    protected GenericService<E,D> service;
    protected GenericRepository<E> repository;
    protected GenericMapperImpl<E,D> mapper;
    @BeforeEach
    void init(){

    }
    protected abstract void getAll();

    protected abstract void getOne() throws NotFoundException;

    protected abstract void create();

    protected abstract void update();

    protected abstract void delete();

}
