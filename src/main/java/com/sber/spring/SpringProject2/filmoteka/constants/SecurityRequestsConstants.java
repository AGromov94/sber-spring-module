package com.sber.spring.SpringProject2.filmoteka.constants;

import java.util.List;

public interface SecurityRequestsConstants {
    List<String> RESOURCES_WHITE_LIST = List.of("/resources/**",
            "/static/**",
            "/js/**",
            "/css/**",
            "/", //выводит на index.html (главная страница сервиса)
            "/swagger-ui/**",
            "/webjars/bootstrap/5.0.2/**",
            "/v3/api-docs/**");

    List<String> FILMS_WHITE_LIST = List.of("/films",
            "/films/search",
            "/films/{id}");

    List<String> FILMS_REST_WHITE_LIST = List.of("/rest/films/list",
            "/rest/films/getFilm/**");

    List<String> DIRECTORS_WHITE_LIST = List.of("/directors",
            "/directors/search",
            "/directors/search/director",
            "/directors/{id}");
    List<String> FILMS_PERMISSION_LIST = List.of("/films/add",
            "/rest/films/addFilm",
            "/rest/films/updateFilm",
            "/rest/films/deleteFilm/{id}",
            "/films/addDirector",
            "/films/update/*",
            "/films/delete/*",
            "/films/download/{filmId}");

    List<String> DIRECTORS_PERMISSION_LIST = List.of("/directors/add",
            "/directors/addDirector",
            "/directors/updateDirector",
            "/directors/addFilm",
            "/directors/deleteDirector/{id}",
            "/directors/update/*",
            "/directors/delete/*");


    List<String> USERS_WHITE_LIST = List.of("/login",
            "/users/registration",
            "/users/remember-password",
            "/users/change-password",
            "/users/auth");

    List<String> USERS_PERMISSION_LIST = List.of("/rent/film/**");
    List<String> ADMINS_PERMISSION_LIST = List.of("/users/list/**");

}
