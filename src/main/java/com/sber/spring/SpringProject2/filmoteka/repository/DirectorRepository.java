package com.sber.spring.SpringProject2.filmoteka.repository;

import com.sber.spring.SpringProject2.filmoteka.model.Director;
import com.sber.spring.SpringProject2.filmoteka.model.Film;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DirectorRepository extends GenericRepository<Director> {
    Page<Director> findAllByDirectorsFioContainsIgnoreCaseAndIsDeletedFalse(String fio, Pageable pageable);
    List<Director> findAllByDirectorsFioContainsIgnoreCaseAndIsDeletedFalse(String fio);
}
