package com.sber.spring.SpringProject2.filmoteka.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class GenericDto {
    protected Integer id;
    protected LocalDateTime createdWhen;
    protected String createdBy;
    protected LocalDateTime updatedWhen;
    protected String updatedBy;
    protected LocalDateTime deletedWhen;
    protected String deletedBy;
    protected boolean isDeleted;
}
