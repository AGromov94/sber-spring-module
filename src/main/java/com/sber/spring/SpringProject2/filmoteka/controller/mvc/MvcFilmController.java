package com.sber.spring.SpringProject2.filmoteka.controller.mvc;

import com.sber.spring.SpringProject2.filmoteka.dto.FilmDto;
import com.sber.spring.SpringProject2.filmoteka.dto.FilmSearchDto;
import com.sber.spring.SpringProject2.filmoteka.dto.FilmWithDirectorsDto;
import com.sber.spring.SpringProject2.filmoteka.exception.MyDeleteException;
import com.sber.spring.SpringProject2.filmoteka.exception.NotFoundException;
import com.sber.spring.SpringProject2.filmoteka.service.FilmService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/*
MVC - model view controller.
Методы MVC контроллеров всегда возвращают String. В результате работы всегда возвращается html страница.
Для POST запросов необходим redirect: в качестве возвращаемого атрибута
 */

@Controller
@RequestMapping("/films")
@RequiredArgsConstructor
public class MvcFilmController {
    private final FilmService filmService;

    @GetMapping("")
    public String listAll(@RequestParam(value = "page", defaultValue = "1") int page,
                          @RequestParam(value = "size", defaultValue = "5") int pageSize,
                          Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "title"));
        List<FilmWithDirectorsDto> filmDtoList = filmService.listAllBooksWithDirectors();
        model.addAttribute("films", filmDtoList);
        return "films/listAllFilms";
    }

    @GetMapping("/{id}")
    public String getDirectorInfo(@PathVariable Integer id,
                                  Model model) throws NotFoundException {
        model.addAttribute("film", filmService.getBookWithDirectorDto(id));
        return "films/viewFilm";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable Integer id,
                         Model model) {
        model.addAttribute("film", filmService.getOne(id));
        return "films/updateFilm";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("filmForm") FilmDto filmDto) {
        filmService.update(filmDto, filmDto.getId());
        return "redirect:/films";
    }

    @GetMapping("/add")
    public String create() {
        return "films/createFilm";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("filmForm") FilmDto filmDto) {
        filmService.create(filmDto);
        return "redirect:/films"; // вызываем метод listAll у MvcFilmController
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Integer id) throws MyDeleteException, NotFoundException {
        filmService.deleteSoft(id);
        return "redirect:/films";
    }

    @GetMapping("/restore/{id}")
    public String restore(@PathVariable Integer id) throws NotFoundException {
        filmService.restore(id);
        return "redirect:/films";
    }

    @PostMapping("/search")
    public String searchBooks(@ModelAttribute("filmSearchForm") FilmSearchDto filmSearchDto,
                              Model model) {
        //PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "title"));
        if (filmSearchDto.getTitle() != null) {
            model.addAttribute("films", filmService.searchFilms(filmSearchDto.getTitle()));
        }
        if (filmSearchDto.getGenre() != null) {
            model.addAttribute("films", filmService.searchFilms(filmSearchDto.getGenre()));
        }
        return "films/listAllFilms";
    }
}
