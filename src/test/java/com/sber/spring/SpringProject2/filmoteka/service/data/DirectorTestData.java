package com.sber.spring.SpringProject2.filmoteka.service.data;

import com.sber.spring.SpringProject2.filmoteka.dto.DirectorDto;
import com.sber.spring.SpringProject2.filmoteka.model.Director;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface DirectorTestData {
    DirectorDto DIRECTOR_DTO_1 = new DirectorDto("DirectorFio1",
            "description1",
            new ArrayList<Integer>());

    DirectorDto DIRECTOR_DTO_2 = new DirectorDto("DirectorFio2",
            "description2",
            new ArrayList<Integer>());

    DirectorDto DIRECTOR_DTO_3_DELETED = new DirectorDto("authorFio3",
            "description3",
            new ArrayList<Integer>());

    List<DirectorDto> DIRECTOR_DTO_LIST = Arrays.asList(DIRECTOR_DTO_1, DIRECTOR_DTO_2, DIRECTOR_DTO_3_DELETED);


    Director DIRECTOR_1 = new Director("director1",
            "description1",
            null);

    Director DIRECTOR_2 = new Director("director2",
            "description2",
            null);

    Director DIRECTOR_3 = new Director("director3",
            "description3",
            null);

    List<Director> DIRECTOR_LIST = Arrays.asList(DIRECTOR_1, DIRECTOR_2, DIRECTOR_3);
}
