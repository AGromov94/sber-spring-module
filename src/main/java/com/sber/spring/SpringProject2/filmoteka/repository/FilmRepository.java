package com.sber.spring.SpringProject2.filmoteka.repository;

import com.sber.spring.SpringProject2.filmoteka.model.Film;
import com.sber.spring.SpringProject2.filmoteka.model.Genre;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

//репозиторий - слой, который хранит запросы к БД. JpaRepository реализует под капотом стандартные CRUD запросы
@Repository
public interface FilmRepository extends GenericRepository<Film> {
    List<Film> findAllByTitleContainsIgnoreCase(String title);
    List<Film> findAllByGenre(Genre genre);

    @Query(nativeQuery = true,
            value = """
                    select distinct b.*
                    from films b
                    left join film_directors ba on b.id = ba.film_id
                    left join directors a on a.id = ba.director_id
                    where cast(b.genre as char) like coalesce(:genre, '%')
                    """)
    List<Film> searchFilms(@Param(value = "genre") String genre);

   /* //пример реализации собственного запроса к БД
     @Query(nativeQuery = true,
    value = """
                select *
                from films
                where title LIKE '%' || :value || '%'
            """)
    List<Film> findFilmsByTitle(final String value);

    //HQL запрос, обращаемся к классу
    @Query(
            value = """
                select b
                from Film b
                where b.title LIKE '%' || :value || '%'
            """)
    List<Film> findFilmsByTitle1(final String value); */
}
