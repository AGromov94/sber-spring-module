//package com.sber.spring.SpringProject2.filmoteka.service;
//
//import org.springframework.stereotype.Service;
//
//@Service
//public class FilmRentService {
//    private final FilmService filmService;
//    private final UserBookRentRepository userBookRentRepository;
//
//    public BookRentService(UserBookRentRepository repository,
//                           UserBookRentMapper mapper,
//                           BookService bookService,
//                           UserBookRentRepository userBookRentRepository) {
//        super(repository, mapper);
//        this.bookService = bookService;
//        this.userBookRentRepository = userBookRentRepository;
//    }
//
//    public void rentABook(final UserBookRentDto userBookRentDto) throws NotFoundException {
//        BookDto bookDto = bookService.getOne(userBookRentDto.getBookId());
//        bookDto.setAmount(bookDto.getAmount() - 1);
//        bookService.update(bookDto, bookDto.getId());
//        int rentPeriod = userBookRentDto.getRentPeriod() != null ? userBookRentDto.getRentPeriod() : 14;
//        userBookRentDto.setRentDate(LocalDate.now());
//        userBookRentDto.setReturned(false);
//        userBookRentDto.setRentPeriod(rentPeriod);
//        userBookRentDto.setCreatedWhen(LocalDateTime.now());
//        userBookRentDto.setReturnDate(LocalDate.now().plusDays(rentPeriod));
//        userBookRentDto.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
//        mapper.toDto(userBookRentRepository.save(mapper.toEntity(userBookRentDto)));
//    }
//
//    public Page<UserBookRentDto> listUserRentBooks(final Long id,
//                                                   final Pageable pageRequest) {
//        Page<UserBookRent> objects = userBookRentRepository.getBookRentInfoByUserId(id, pageRequest);
//        List<UserBookRentDto> results = mapper.toDtos(objects.getContent());
//        return new PageImpl<>(results, pageRequest, objects.getTotalElements());
//    }
//
//    public void returnBook(final Long id) {
//        UserBookRentDto bookRentInfoDTO = getOne(id);
//        bookRentInfoDTO.setReturned(true);
//        bookRentInfoDTO.setReturnDate(LocalDate.now());
//        BookDto bookDTO = bookRentInfoDTO.getBookDTO();
//        bookDTO.setAmount(bookDTO.getAmount() + 1);
//        update(bookRentInfoDTO, id);
//        bookService.update(bookDTO, bookDTO.getId());
//    }
//
//}
