package com.sber.spring.SpringProject2.filmoteka.repository;

import com.sber.spring.SpringProject2.filmoteka.model.Order;
import com.sber.spring.SpringProject2.filmoteka.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends GenericRepository<Order> {

}
