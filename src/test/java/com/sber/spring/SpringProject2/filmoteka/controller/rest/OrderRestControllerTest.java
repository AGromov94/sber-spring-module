package com.sber.spring.SpringProject2.filmoteka.controller.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sber.spring.SpringProject2.filmoteka.dto.OrderDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.util.List;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@RequiredArgsConstructor
public class OrderRestControllerTest extends GenericControllerTest {
    @Test
    @Override
    protected void createObject() throws Exception {
        OrderDto adderOrder = new OrderDto(1,
                1,
                LocalDate.of(2000, 2, 22),
                1,
                LocalDate.of(2001, 2, 22),
                true);
        log.info("Testing createObject orders via REST started");
        String result = mvc.perform(
                        MockMvcRequestBuilders.post("/rest/orders/addOrder")
                                .content(super.asJsonString(adderOrder))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful()) //проверяем что статус ответа 200
                .andExpect(jsonPath("$.*", hasSize(greaterThan(0)))) //проверяем размер полученного JSONа
                .andReturn()
                .getResponse()
                .getContentAsString();
        assertEquals(1, objectMapper.readValue(result, new TypeReference<OrderDto>() {
        }).getRentPeriod());
        log.info(adderOrder.toString());
        log.info("Testing createObject orders via REST finished");

    }

    @Test
    @Override
    protected void updateObject() throws Exception {
        OrderDto updatedOrder = new OrderDto(2,
                2,
                LocalDate.of(2000, 2, 22),
                1,
                LocalDate.of(2001, 2, 22),
                true);
        log.info("Testing updateObject orders via REST started");
        String result = mvc.perform(
                        MockMvcRequestBuilders.put("/rest/orders/updateOrder")
                                .content(super.asJsonString(updatedOrder))
                                .param("id", "4")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful()) //проверяем что статус ответа 200
                .andExpect(jsonPath("$.*", hasSize(greaterThan(0)))) //проверяем размер полученного JSONа
                .andReturn()
                .getResponse()
                .getContentAsString();
        assertEquals(2, objectMapper.readValue(result, new TypeReference<OrderDto>() {
        }).getFilmId());
        log.info(objectMapper.readValue(result, new TypeReference<OrderDto>() {
        }).toString());
        log.info("Testing updateObject orders via REST finished");
    }

    @Test
    @Override
    protected void deleteObject() throws Exception {
        log.info("Testing deleteObject orders via REST started");
        String result1 = mvc.perform(
                        MockMvcRequestBuilders.get("/rest/orders/getListOfOrders")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse()
                .getContentAsString();
        super.mvc.perform(MockMvcRequestBuilders
                        .delete("/rest/orders/deleteOrder/3")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        String result2 = mvc.perform(
                        MockMvcRequestBuilders.get("/rest/orders/getListOfOrders")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse()
                .getContentAsString();
        assertNotEquals(result1.length(), result2.length()); //до удаления != после удаления
        log.info("Testing deleteObject orders via REST finished");
    }

    @Test
    @Override
    protected void listAll() throws Exception {
        log.info("Testing listAll orders via REST started");
        String result = mvc.perform(
                        MockMvcRequestBuilders.get("/rest/orders/getListOfOrders")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful()) //проверяем что статус ответа 200
                .andExpect(jsonPath("$.*", hasSize(greaterThan(0)))) //проверяем размер полученного JSONа
                .andReturn()
                .getResponse()
                .getContentAsString();
        List<OrderDto> orderDtos = objectMapper.readValue(result, new TypeReference<List<OrderDto>>() {
        });
        assertFalse(orderDtos.isEmpty()); //проверяем что список режиссеров не пустой
        for (OrderDto o : orderDtos) {
            log.info("Order: " + o.toString());
        }
        log.info("Testing listAll orders via REST finished");
    }

    @Test
    @Override
    protected void listOne() throws Exception {
        log.info("Testing listOne orders via REST started");
        String result = mvc.perform(
                        MockMvcRequestBuilders.get("/rest/orders/getOrder")
                                .param("id", "1")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful()) //проверяем что статус ответа 200
                .andExpect(jsonPath("$.*", hasSize(greaterThan(0)))) //проверяем размер полученного JSONа
                .andReturn()
                .getResponse()
                .getContentAsString();
        OrderDto orderDto = objectMapper.readValue(result, new TypeReference<OrderDto>() {
        });
        assertEquals(1, orderDto.getUserId());
        log.info(orderDto.toString());
        log.info("Testing listOne orders via REST finished");
    }
}