CREATE TABLE clients (
    id serial primary key,
    last_name varchar not null ,
    first_name varchar not null ,
    date_of_birth date not null ,
    phone_number varchar not null ,
    books_id Integer[]
);

CREATE TABLE books (
    id serial primary key,
    title varchar(30) not null ,
    author varchar(30) not null ,
    date_added timestamp not null
);

select * from clients;

select * from books;