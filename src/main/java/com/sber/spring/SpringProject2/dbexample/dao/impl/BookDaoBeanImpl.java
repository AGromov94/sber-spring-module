package com.sber.spring.SpringProject2.dbexample.dao.impl;

import com.sber.spring.SpringProject2.dbexample.model.Book;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
@Component
public class BookDaoBeanImpl {
    private static final String GET_BOOK_BY_ID_QUERY = """
            select * from books where id = ?
            """;

    private static final String GET_BOOKS_LIST_QUERY = """
            select * from books
            """;
    private final Connection connection;

    public BookDaoBeanImpl(Connection connection) {
        this.connection = connection;
    }

    public Book getOneObjectById(Integer id) throws SQLException {
        PreparedStatement query = connection.prepareStatement(GET_BOOK_BY_ID_QUERY);
        query.setInt(1, id);
        ResultSet result = query.executeQuery();

        Book book = new Book(); //потому что метод вернет 1 строчку можно создать объект вне цикла while
        while (result.next()) {
            book.setId(result.getInt("id"));
            book.setAuthor(result.getString("author"));
            book.setTitle(result.getString("title"));
            book.setDateAdded(result.getDate("date_added"));
        }
        return book;
    }

    public List<Book> getAllObjects() throws SQLException {
        PreparedStatement query = connection.prepareStatement(GET_BOOKS_LIST_QUERY);
        ResultSet result = query.executeQuery();
        List<Book> books = new ArrayList<>();
        while (result.next()) {
            Book book = new Book();
            book.setId(result.getInt("id"));
            book.setAuthor(result.getString("author"));
            book.setTitle(result.getString("title"));
            book.setDateAdded(result.getDate("date_added"));
            books.add(book);
        }
        return books;
    }
}
