package com.sber.spring.SpringProject2.filmoteka.service.data;

import com.sber.spring.SpringProject2.filmoteka.dto.OrderDto;
import com.sber.spring.SpringProject2.filmoteka.model.Film;
import com.sber.spring.SpringProject2.filmoteka.model.Order;
import com.sber.spring.SpringProject2.filmoteka.model.User;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public interface OrderTestData {
    OrderDto ORDER_DTO_1 = new OrderDto(1,
            1,
            LocalDate.of(1990, 1, 1),
            2,
            LocalDate.of(1992, 1, 1),
            true);

    OrderDto ORDER_DTO_2 = new OrderDto(2,
            2,
            LocalDate.of(1990, 1, 1),
            2,
            LocalDate.of(1992, 1, 1),
            true);

    OrderDto ORDER_DTO_3 = new OrderDto(3,
            3,
            LocalDate.of(1990, 1, 1),
            2,
            LocalDate.of(1992, 1, 1),
            true);

    List<OrderDto> ORDER_DTO_LIST = Arrays.asList(ORDER_DTO_1, ORDER_DTO_2, ORDER_DTO_3);


    Order ORDER_1 = new Order(new User(),
            new Film(),
            LocalDate.of(1990, 1, 1),
            2,
            LocalDate.of(1992, 1, 1),
            true);

    Order ORDER_2 = new Order(new User(),
            new Film(),
            LocalDate.of(1990, 1, 1),
            2,
            LocalDate.of(1992, 1, 1),
            true);

    Order ORDER_3 = new Order(new User(),
            new Film(),
            LocalDate.of(1990, 1, 1),
            2,
            LocalDate.of(1992, 1, 1),
            true);

    List<Order> ORDER_LIST = Arrays.asList(ORDER_1, ORDER_2, ORDER_3);
}
