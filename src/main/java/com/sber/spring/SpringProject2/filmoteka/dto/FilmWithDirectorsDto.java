package com.sber.spring.SpringProject2.filmoteka.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class FilmWithDirectorsDto extends FilmDto {
    private Set<DirectorDto> directors;
}
