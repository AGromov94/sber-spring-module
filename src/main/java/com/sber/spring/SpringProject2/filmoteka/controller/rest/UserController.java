package com.sber.spring.SpringProject2.filmoteka.controller.rest;

import com.sber.spring.SpringProject2.filmoteka.dto.*;
import com.sber.spring.SpringProject2.filmoteka.exception.NotFoundException;
import com.sber.spring.SpringProject2.filmoteka.model.Film;
import com.sber.spring.SpringProject2.filmoteka.model.Order;
import com.sber.spring.SpringProject2.filmoteka.model.User;
import com.sber.spring.SpringProject2.filmoteka.repository.FilmRepository;
import com.sber.spring.SpringProject2.filmoteka.repository.UserRepository;
import com.sber.spring.SpringProject2.filmoteka.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/rest/users")
@Slf4j
@Tag(name = "Пользователи", description = "Контроллер для работы с пользователями")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @Operation(description = "Получить список всех пользователей")
    @RequestMapping(value = "/getListOfUsers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UserDto>> getAllUsers() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(userService.listAll());
    }

    @Operation(description = "Получить пользователя по id")
    @RequestMapping(value = "/getUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> getOneUser(@RequestParam(value = "id") Integer id) throws NotFoundException {
        return ResponseEntity.status(HttpStatus.OK)
                .body(userService.getOne(id));
    }

//    @Operation(description = "Добавить нового пользователя")
//    @RequestMapping(value = "/addUser", method = RequestMethod.POST,
//            produces = MediaType.APPLICATION_JSON_VALUE,
//            consumes = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<UserDto> createUser(@RequestBody UserDto newUser) {
//        return ResponseEntity.status(HttpStatus.CREATED)
//                .body(userService.createUserByRequest(newUser));
//    }

    @Operation(description = "Обновить пользователя по id")
    @RequestMapping(value = "/updateUser", method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> updateUser(@RequestBody UserDto updatedUser, @RequestParam(value = "id") Integer id) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(userService.update(updatedUser, id));
    }

    @Operation(description = "Удалить пользователя")
    @RequestMapping(value = "/deleteUser/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(value = "id") Integer id) {
        userService.delete(id);
    }

    @Operation(description = "Получить список заказов пользователя")
    @RequestMapping(value = "/getUsersOrders", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Integer>> getOrders(@RequestParam(value = "id") Integer id) {
        return ResponseEntity.status(HttpStatus.OK).body(userService.getOrders(id));
    }

    @Operation(description = "Получить список арендованных/купленных фильмов у пользователя")
    @RequestMapping(value = "/getUsersFilms", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<String>> getUsersFilms(@RequestParam(value = "id") Integer id) {
        return ResponseEntity.status(HttpStatus.OK).body(userService.getUsersFilms(id));
    }
}
