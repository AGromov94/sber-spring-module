package com.sber.spring.SpringProject2.filmoteka.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
//Класс для настройки swagger
@Configuration
public class OpenApiConfig {
    @Bean
    public OpenAPI filmotekaProject() {
        return new OpenAPI().info(new Info()
                .title("API Фильмотеки")
                .description("Сервис аренды фильмов онлайн")
                .version("1.0")
                .license(new License().name("Apache 2.0").url("http://kakoitourl.org"))
                .contact(new Contact().name("Andrei Gromov")
                        .email("andrey.gromov1994@mail.ru"))
        );
    }
}
