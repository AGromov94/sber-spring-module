//package com.sber.spring.SpringProject2.filmoteka.service;
//
//import com.sber.spring.SpringProject2.filmoteka.dto.AddFilmDto;
//import com.sber.spring.SpringProject2.filmoteka.dto.DirectorDto;
//import com.sber.spring.SpringProject2.filmoteka.exception.NotFoundException;
//import com.sber.spring.SpringProject2.filmoteka.mapper.DirectorMapper;
//import com.sber.spring.SpringProject2.filmoteka.model.Director;
//import com.sber.spring.SpringProject2.filmoteka.repository.DirectorRepository;
//import com.sber.spring.SpringProject2.filmoteka.service.data.DirectorTestData;
//import lombok.extern.slf4j.Slf4j;
//import org.junit.jupiter.api.Order;
//import org.junit.jupiter.api.Test;
//import org.mockito.Mockito;
//
//import java.util.List;
//import java.util.Optional;
//
//import static org.junit.jupiter.api.Assertions.*;
//import static org.mockito.ArgumentMatchers.eq;
//import static org.mockito.Mockito.times;
//import static org.mockito.Mockito.verify;
//
//@Slf4j
//public class DirectorServiceTest extends GenericTest<Director, DirectorDto> {
//    public DirectorServiceTest() {
//        repository = Mockito.mock(DirectorRepository.class);
//        mapper = Mockito.mock(DirectorMapper.class);
//        service = new DirectorService((DirectorRepository) repository, (DirectorMapper) mapper);
//    }
//
//    @Order(3)
//    @Test
//    @Override
//    protected void getAll() {
//        Mockito.when(repository.findAll()).thenReturn(DirectorTestData.DIRECTOR_LIST);
//        Mockito.when(mapper.toDtos(DirectorTestData.DIRECTOR_LIST)).thenReturn(DirectorTestData.DIRECTOR_DTO_LIST);
//        List<DirectorDto> directorDtos = service.listAll();
//        log.info("Testing getAll(): " + directorDtos.toString());
//        assertEquals(directorDtos.size(), DirectorTestData.DIRECTOR_LIST.size());
//    }
//
//    @Order(2)
//    @Test
//    @Override
//    protected void getOne() throws NotFoundException {
//        Director director1 = DirectorTestData.DIRECTOR_1;
//        director1.setId(1);
//        DirectorDto authorDtoInitial1 = DirectorTestData.DIRECTOR_DTO_1;
//        authorDtoInitial1.setId(1);
//
//        Director director2 = DirectorTestData.DIRECTOR_2;
//        director2.setId(2);
//        DirectorDto authorDtoInitial2 = DirectorTestData.DIRECTOR_DTO_2;
//        authorDtoInitial2.setId(2);
//
//        Mockito.when(repository.findById(1)).thenReturn(Optional.of(director1));
//        Mockito.when(repository.findById(2)).thenReturn(Optional.of(director2));
//        Mockito.when(mapper.toDto(director1)).thenReturn(authorDtoInitial1);
//        Mockito.when(mapper.toDto(director2)).thenReturn(authorDtoInitial2);
//
//        DirectorDto directorDto1 = service.getOne(1);
//        DirectorDto directorDto2 = service.getOne(2);
//        log.info("Testing getOne(): " + directorDto1);
//        log.info("Testing getOne(): " + directorDto2);
//        assertEquals(directorDto1, DirectorTestData.DIRECTOR_DTO_1);
//        assertNotEquals(directorDto2, DirectorTestData.DIRECTOR_DTO_1);
//    }
//
//    @Order(1)
//    @Test
//    @Override
//    protected void create() {
//        Mockito.when(mapper.toEntity(DirectorTestData.DIRECTOR_DTO_1)).thenReturn(DirectorTestData.DIRECTOR_1);
//        Mockito.when(mapper.toDto(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_DTO_1);
//        Mockito.when(repository.save(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_1);
//        DirectorDto directorDto = service.create(DirectorTestData.DIRECTOR_DTO_1);
//        log.info("Testing create(): " + directorDto);
//        assertEquals(directorDto, DirectorTestData.DIRECTOR_DTO_1);
//    }
//
//    @Order(4)
//    @Test
//    @Override
//    protected void update() {
//        Mockito.when(mapper.toEntity(DirectorTestData.DIRECTOR_DTO_1)).thenReturn(DirectorTestData.DIRECTOR_1);
//        Mockito.when(mapper.toDto(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_DTO_1);
//        Mockito.when(repository.save(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_1);
//        DirectorDto directorDto = service.update(DirectorTestData.DIRECTOR_DTO_1, 1);
//        log.info("Testing update(): " + directorDto);
//        assertEquals(directorDto, DirectorTestData.DIRECTOR_DTO_1);
//    }
//
//    @Order(5)
//    @Test
//    @Override
//    protected void delete() {
//        Mockito.doNothing().when(repository).deleteById(1);
//        service.delete(1);
//        verify(repository, times(1)).deleteById(eq(1));
//    }
//
////    @Test
////    protected void delete1() {
////        Integer id = 1;
////        repository.deleteById(id);
////        verify(repository, times(1)).deleteById(eq(id));
////    }
//
//    @Order(6)
//    @Test
//    protected void addFilm() throws NotFoundException {
//        Mockito.when(repository.findById(1)).thenReturn(Optional.of(DirectorTestData.DIRECTOR_1));
//        Mockito.when(service.getOne(1)).thenReturn(DirectorTestData.DIRECTOR_DTO_1);
//        Mockito.when(repository.save(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_1);
//        ((DirectorService) service).addFilm(new AddFilmDto(1, 1));
//        log.info("Testing addFilm(): " + DirectorTestData.DIRECTOR_DTO_1.getFilmIds());
//        log.info("Testing addFilm(): " + DirectorTestData.DIRECTOR_DTO_2.getFilmIds());
//        assertFalse(DirectorTestData.DIRECTOR_DTO_1.getFilmIds().isEmpty());
//        assertNotEquals(DirectorTestData.DIRECTOR_DTO_1.getFilmIds().size(), DirectorTestData.DIRECTOR_DTO_2.getFilmIds().size());
//    }
//
//    @Test
//    @Order(7)
//    protected void searchDirectorsTest() {
//        String fio = "fio";
//        Mockito.when(((DirectorRepository) repository).findAllByDirectorsFioContainsIgnoreCase(fio)).thenReturn(DirectorTestData.DIRECTOR_LIST);
//        Mockito.when(mapper.toDtos(DirectorTestData.DIRECTOR_LIST)).thenReturn(DirectorTestData.DIRECTOR_DTO_LIST);
//        List<DirectorDto> directorDtoList = ((DirectorService) service).searchDirectors(fio);
//        log.info("Testing update(): " + directorDtoList);
//        assertEquals(directorDtoList, DirectorTestData.DIRECTOR_DTO_LIST);
//    }
//}
