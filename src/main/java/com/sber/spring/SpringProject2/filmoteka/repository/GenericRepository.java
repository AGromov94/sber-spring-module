package com.sber.spring.SpringProject2.filmoteka.repository;

import com.sber.spring.SpringProject2.filmoteka.model.GenericModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

/**
 * Aбстрактный репозиторий
 * Необходим для работы абстрактного сервиса
 * т.к. в абстрактном сервисе мы не используем конкретный репозиторий
 *
 * @param <T> - сущность с которой работает репозиторий (наследуется от GenericModel)
 */
@NoRepositoryBean
public interface GenericRepository<T extends GenericModel> extends JpaRepository<T, Integer> {
    Page<T> findAllByIsDeletedFalse(Pageable pageable);

    List<T> findAllByIsDeletedFalse();

}
