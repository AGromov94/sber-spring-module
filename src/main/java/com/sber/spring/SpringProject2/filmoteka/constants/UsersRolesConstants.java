package com.sber.spring.SpringProject2.filmoteka.constants;

public interface UsersRolesConstants {
    String USER = "USER";
    String LIBRARIAN = "LIBRARIAN";
}
