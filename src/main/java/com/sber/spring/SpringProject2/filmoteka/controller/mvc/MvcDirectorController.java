package com.sber.spring.SpringProject2.filmoteka.controller.mvc;

import com.sber.spring.SpringProject2.filmoteka.dto.DirectorDto;
import com.sber.spring.SpringProject2.filmoteka.exception.MyDeleteException;
import com.sber.spring.SpringProject2.filmoteka.exception.NotFoundException;
import com.sber.spring.SpringProject2.filmoteka.service.DirectorService;
import com.sber.spring.SpringProject2.filmoteka.service.FilmService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/*
MVC - model view controller.
Методы MVC контроллеров всегда возвращают String. В результате работы всегда возвращается html страница.
Для POST запросов необходим redirect: в качестве возвращаемого атрибута
 */

@Controller
@RequestMapping("/directors")
@RequiredArgsConstructor
public class MvcDirectorController {
    private final DirectorService directorService;

    @GetMapping("")
    public String listAll(@RequestParam(value = "page", defaultValue = "1") int page,
                          @RequestParam(value = "size", defaultValue = "5") int pageSize,
                          Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "directorsFio"));
        List<DirectorDto> directorDtoList = directorService.listAllNotDeleted();
        model.addAttribute("directors", directorDtoList);
        return "directors/listAllDirectors";
    }

    @GetMapping("/{id}")
    public String getDirectorInfo(@PathVariable Integer id,
                                  Model model) {
        model.addAttribute("director", directorService.getOne(id));
        return "directors/viewDirector";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable Integer id,
                         Model model) {
        model.addAttribute("director", directorService.getOne(id));
        return "directors/updateDirector";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("directorForm") DirectorDto directorDto) {
        directorService.update(directorDto, directorDto.getId());
        return "redirect:/directors";
    }

    @GetMapping("/add")
    public String create() {
        return "directors/createDirector";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("directorForm") DirectorDto directorDto) {
        directorService.create(directorDto);
        return "redirect:/directors"; // вызываем метод listAll у MvcDirectorController
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Integer id) throws MyDeleteException, NotFoundException {
        directorService.deleteSoft(id);
        return "redirect:/directors";
    }

    @GetMapping("/restore/{id}")
    public String restore(@PathVariable Integer id) throws NotFoundException {
        directorService.restore(id);
        return "redirect:/directors";
    }

    @PostMapping("/search")
    public String searchDirectors(@ModelAttribute("directorSearchForm") DirectorDto directorDto,
                                  Model model) {
        //PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "directorsFio"));
        model.addAttribute("directors", directorService.searchDirectors(directorDto.getDirectorsFio()));
        return "directors/listAllDirectors";
    }
}
