package com.sber.spring.SpringProject2.filmoteka.controller.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sber.spring.SpringProject2.filmoteka.dto.RoleDto;
import com.sber.spring.SpringProject2.filmoteka.dto.UserDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@RequiredArgsConstructor
public class UserRestControllerTest extends GenericControllerTest {
    @Test
    @Order(3)
    @Override
    protected void createObject() throws Exception {
        UserDto addedUser = new UserDto(
                "AAA",
                "AAA",
                "AAA",
                "AAA",
                "AAA",
                LocalDate.of(1990, 9, 19),
                "777",
                "AAAA",
                "AAA",
                new RoleDto(),
                new ArrayList<>(),
                "");
        log.info("Testing createObject users via REST started");
        String result = mvc.perform(
                        MockMvcRequestBuilders.post("/rest/users/addUser")
                                .content(super.asJsonString(addedUser))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful()) //проверяем что статус ответа 200
                .andExpect(jsonPath("$.*", hasSize(greaterThan(0)))) //проверяем размер полученного JSONа
                .andReturn()
                .getResponse()
                .getContentAsString();
        assertEquals("AAAA", objectMapper.readValue(result, new TypeReference<UserDto>() {
        }).getAddress());
        assertEquals("777", objectMapper.readValue(result, new TypeReference<UserDto>() {
        }).getPhone());
        log.info(addedUser.toString());
        log.info("Testing createObject users via REST finished");
    }

    @Test
    @Order(4)
    @Override
    protected void updateObject() throws Exception {
        UserDto updatedUser = new UserDto(
                "BBB",
                "BBB",
                "BBB",
                "BBB",
                "BBB",
                LocalDate.of(1990, 9, 19),
                "888",
                "BBB",
                "BBB",
                new RoleDto(),
                new ArrayList<>(),
                "");
        log.info("Testing updateObject users via REST started");
        String result = mvc.perform(
                        MockMvcRequestBuilders.put("/rest/users/updateUser")
                                .content(super.asJsonString(updatedUser))
                                .param("id", "54")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful()) //проверяем что статус ответа 200
                .andExpect(jsonPath("$.*", hasSize(greaterThan(0)))) //проверяем размер полученного JSONа
                .andReturn()
                .getResponse()
                .getContentAsString();
        assertEquals("BBB", objectMapper.readValue(result, new TypeReference<UserDto>() {
        }).getPassword());
        assertEquals("BBB", objectMapper.readValue(result, new TypeReference<UserDto>() {
        }).getLogin());
        log.info(objectMapper.readValue(result, new TypeReference<UserDto>() {
        }).toString());
        log.info("Testing updateObject users via REST finished");
    }

    @Test
    @Order(5)
    @Override
    protected void deleteObject() throws Exception {
        log.info("Testing deleteObject users via REST started");
        String result1 = mvc.perform(
                        MockMvcRequestBuilders.get("/rest/users/getListOfUsers")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse()
                .getContentAsString();
        super.mvc.perform(MockMvcRequestBuilders
                        .delete("/rest/users/deleteUser/53")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        String result2 = mvc.perform(
                        MockMvcRequestBuilders.get("/rest/users/getListOfUsers")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse()
                .getContentAsString();
        assertNotEquals(result1.length(), result2.length()); //до удаления != после удаления
        log.info("Testing deleteObject users via REST finished");
    }

    @Test
    @Order(1)
    @Override
    protected void listAll() throws Exception {
        log.info("Testing listAll users via REST started");
        String result = mvc.perform(
                        MockMvcRequestBuilders.get("/rest/users/getListOfUsers")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful()) //проверяем что статус ответа 200
                .andExpect(jsonPath("$.*", hasSize(greaterThan(0)))) //проверяем размер полученного JSONа
                .andReturn()
                .getResponse()
                .getContentAsString();
        List<UserDto> userDtos = objectMapper.readValue(result, new TypeReference<List<UserDto>>() {
        });
        assertFalse(userDtos.isEmpty()); //проверяем что список режиссеров не пустой
        for (UserDto u : userDtos) {
            log.info("User: " + u.toString());
        }
        log.info("Testing listAll users via REST finished");
    }

    @Test
    @Order(2)
    @Override
    protected void listOne() throws Exception {
        log.info("Testing listOne users via REST started");
        String result = mvc.perform(
                        MockMvcRequestBuilders.get("/rest/users/getUser")
                                .param("id", "1")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful()) //проверяем что статус ответа 200
                .andExpect(jsonPath("$.*", hasSize(greaterThan(0)))) //проверяем размер полученного JSONа
                .andReturn()
                .getResponse()
                .getContentAsString();
        UserDto userDto = objectMapper.readValue(result, new TypeReference<UserDto>() {
        });
        assertEquals("login", userDto.getLogin());
        assertEquals("password", userDto.getPassword());
        log.info(userDto.toString());
        log.info("Testing listOne users via REST finished");
    }
}
