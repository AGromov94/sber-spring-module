package com.sber.spring.SpringProject2.dbexample.costants;

public interface DataBaseConstants {
    String DB_USER = "postgres";
    String DB_PASSWORD = "12345";
    String DB_HOST = "localhost";
    String DB_PORT = "5432";
    String DB_NAME = "local_db";
}
