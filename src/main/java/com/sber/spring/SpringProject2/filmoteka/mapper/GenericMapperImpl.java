package com.sber.spring.SpringProject2.filmoteka.mapper;

import com.sber.spring.SpringProject2.filmoteka.dto.GenericDto;
import com.sber.spring.SpringProject2.filmoteka.model.GenericModel;
import jakarta.annotation.PostConstruct;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

@Component
public abstract class GenericMapperImpl<E extends GenericModel, D extends GenericDto> implements Mapper<E, D> {
    private final Class<E> entityClass;
    private final Class<D> dtoClass;
    protected final ModelMapper modelMapper;

    public GenericMapperImpl(Class<E> entityClass, Class<D> dtoClass, ModelMapper modelMapper) {
        this.entityClass = entityClass;
        this.dtoClass = dtoClass;
        this.modelMapper = modelMapper;
    }

    @Override
    public E toEntity(GenericDto dto) {
        return Objects.isNull(dto) ? null : modelMapper.map(dto, entityClass);
    }

    @Override
    public D toDto(GenericModel entity) {
        return Objects.isNull(entity) ? null : modelMapper.map(entity, dtoClass);
    }

    @Override
    public List<E> toEntities(List<D> dtoList) {
        return dtoList.stream().map(this::toEntity).toList();
    }

    @Override
    public List<D> toDtos(List<E> entityList) {
        return entityList.stream().map(this::toDto).toList();
    }

    /**
     * означает что метод будет применен после основного маппинга (будут заполняться поля null, не заполненные после основного маппинга)
     */
    @PostConstruct
    protected abstract void setupMapper();

    protected Converter<E, D> toDtoConverter(){
        return contex -> {
            E source = contex.getSource();
            D destination = contex.getDestination();
            mapSpecificFields(source, destination);
            return contex.getDestination();
        };
    }

    protected Converter<D, E> toEntityConverter(){
        return contex -> {
            D source = contex.getSource();
            E destination = contex.getDestination();
            mapSpecificFields(source, destination);
            return contex.getDestination();
        };
    }

    protected abstract void  mapSpecificFields(E source, D destination);
    protected abstract void  mapSpecificFields(D source, E destination);

    protected abstract List<Integer> fillIds(E source);
}
