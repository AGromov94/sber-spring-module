package com.sber.spring.SpringProject2.filmoteka.dto;

import com.sber.spring.SpringProject2.filmoteka.model.Director;
import com.sber.spring.SpringProject2.filmoteka.model.Film;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DirectorDto extends GenericDto {
    private String directorsFio;
    private String position;
    private List<Integer> filmIds = new ArrayList<>();
}
