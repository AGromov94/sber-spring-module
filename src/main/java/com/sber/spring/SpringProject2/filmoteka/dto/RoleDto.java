package com.sber.spring.SpringProject2.filmoteka.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RoleDto {
    private Integer id;
    private String title;
    private String description;
}
