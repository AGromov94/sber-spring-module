package com.sber.spring.SpringProject2.filmoteka.controller.rest;

import com.sber.spring.SpringProject2.filmoteka.dto.AddDirectorDto;
import com.sber.spring.SpringProject2.filmoteka.dto.FilmDto;
import com.sber.spring.SpringProject2.filmoteka.exception.NotFoundException;
import com.sber.spring.SpringProject2.filmoteka.service.FilmService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//сразу становится бином на уровне (слое) контроллеров
@RestController
@RequiredArgsConstructor
@RequestMapping("/rest/films")
@Slf4j
@Tag(name = "Фильмы", description = "Контроллер для работы с фильмами") //swagger openapi настройка
public class FilmController {
    private final FilmService filmService;

    @Operation(description = "Получить список всех фильмов") //swagger openapi настройка
    @RequestMapping(value = "/getListOfFilms", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    //тип запроса - get
    public ResponseEntity<List<FilmDto>> getAllFilms() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(filmService.listAll());
    }

    @Operation(description = "Получить фильм по id")
    @RequestMapping(value = "/getFilm", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FilmDto> getOneFilm(@RequestParam(value = "id") Integer id) throws NotFoundException {
        return ResponseEntity.status(HttpStatus.OK)
                .body(filmService.getOne(id));
    }

    @Operation(description = "Добавить новый фильм")
    @RequestMapping(value = "/addFilm", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FilmDto> createFilm(@RequestBody FilmDto newFilm) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(filmService.create(newFilm));
    }

    @Operation(description = "Обновить фильм по id")
    @RequestMapping(value = "/updateFilm", method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FilmDto> updateFilm(@RequestBody FilmDto updatedFilm, @RequestParam(value = "id") Integer id) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(filmService.update(updatedFilm, id));
    }

    //@RequestParam: localhost:9098/films/deleteFilm?id=1
//    @RequestMapping(value = "/deleteFilm", method = RequestMethod.DELETE)
//    public void delete(@RequestParam(value = "id") Integer id){
//        filmRepository.deleteById(id);
//    }
// Два способа обратиться к маппингу через параметр
    //@PathVariable: localhost:9098/films/deleteFilm/1
    @Operation(description = "Удалить фильм")
    @RequestMapping(value = "/deleteFilm/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(value = "id") Integer id) {
        filmService.delete(id);
    }

    @Operation(description = "Добавить режиссера к фильму")
    @RequestMapping(value = "/addDirector", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FilmDto> addDirector(@RequestBody AddDirectorDto addDirectorDto) throws NotFoundException {
        return ResponseEntity.status(HttpStatus.CREATED).body(filmService.addDirector(addDirectorDto));
    }

//    @Operation(description = "Добавить режиссера к фильму")
//    @RequestMapping(value = "/addDirector", method = RequestMethod.POST,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<FilmDto> addDirector(@RequestBody AddFilmDto addDirectorDto) throws NotFoundException {
//        return ResponseEntity.status(HttpStatus.CREATED).body(filmService.addFilm(addDirectorDto));
//    }
}
