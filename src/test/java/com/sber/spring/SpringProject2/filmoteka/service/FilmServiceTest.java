package com.sber.spring.SpringProject2.filmoteka.service;

import com.sber.spring.SpringProject2.filmoteka.dto.FilmDto;
import com.sber.spring.SpringProject2.filmoteka.dto.FilmWithDirectorsDto;
import com.sber.spring.SpringProject2.filmoteka.exception.NotFoundException;
import com.sber.spring.SpringProject2.filmoteka.mapper.FilmMapper;
import com.sber.spring.SpringProject2.filmoteka.mapper.FilmWithDirectorsMapper;
import com.sber.spring.SpringProject2.filmoteka.model.Film;
import com.sber.spring.SpringProject2.filmoteka.repository.FilmRepository;
import com.sber.spring.SpringProject2.filmoteka.service.data.FilmTestData;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@Slf4j
public class FilmServiceTest extends GenericTest<Film, FilmDto> {
    public FilmServiceTest() {
        repository = Mockito.mock(FilmRepository.class);
        mapper = Mockito.mock(FilmMapper.class);
        FilmWithDirectorsMapper mapper1 = Mockito.mock(FilmWithDirectorsMapper.class);
        service = new FilmService((FilmRepository) repository, (FilmMapper) mapper, (FilmWithDirectorsMapper) mapper1);
    }

    @Test
    @Order(3)
    @Override
    protected void getAll() {
        Mockito.when(repository.findAll()).thenReturn(FilmTestData.FILM_LIST);
        Mockito.when(mapper.toDtos(FilmTestData.FILM_LIST)).thenReturn(FilmTestData.FILM_DTO_LIST);
        List<FilmDto> filmDtoList = service.listAll();
        log.info("Testing getAll(): " + filmDtoList.toString());
        assertEquals(filmDtoList.size(), FilmTestData.FILM_LIST.size());
    }

    @Test
    @Order(2)
    @Override
    protected void getOne() throws NotFoundException {
        Film film1 = FilmTestData.FILM_1;
        film1.setId(1);
        FilmDto filmDtoInitial1 = FilmTestData.FILM_DTO_1;
        filmDtoInitial1.setId(1);

        Film film2 = FilmTestData.FILM_2;
        film1.setId(2);
        FilmDto filmDtoInitial2 = FilmTestData.FILM_DTO_2;
        filmDtoInitial2.setId(2);

        Mockito.when(repository.findById(1)).thenReturn(Optional.of(film1));
        Mockito.when(repository.findById(2)).thenReturn(Optional.of(film2));
        Mockito.when(mapper.toDto(film1)).thenReturn(filmDtoInitial1);
        Mockito.when(mapper.toDto(film2)).thenReturn(filmDtoInitial2);

        FilmDto filmDto1 = service.getOne(1);
        FilmDto filmDto2 = service.getOne(2);

        log.info("Testing getOne(): " + filmDto1);
        log.info("Testing getOne(): " + filmDto2);
        assertEquals(filmDto1, FilmTestData.FILM_DTO_1);
        assertNotEquals(filmDto1, FilmTestData.FILM_DTO_2);
    }

    @Test
    @Order(1)
    @Override
    protected void create() {
        Mockito.when(mapper.toEntity(FilmTestData.FILM_DTO_1)).thenReturn(FilmTestData.FILM_1);
        Mockito.when(mapper.toDto(FilmTestData.FILM_1)).thenReturn(FilmTestData.FILM_DTO_1);
        Mockito.when(repository.save(FilmTestData.FILM_1)).thenReturn(FilmTestData.FILM_1);
        FilmDto filmDto = service.create(FilmTestData.FILM_DTO_1);
        log.info("Testing create(): " + filmDto);
        assertEquals(filmDto, FilmTestData.FILM_DTO_1);
    }

    @Test
    @Order(4)
    @Override
    protected void update() {
        Mockito.when(mapper.toEntity(FilmTestData.FILM_DTO_1)).thenReturn(FilmTestData.FILM_1);
        Mockito.when(mapper.toDto(FilmTestData.FILM_1)).thenReturn(FilmTestData.FILM_DTO_1);
        Mockito.when(repository.save(FilmTestData.FILM_1)).thenReturn(FilmTestData.FILM_1);
        FilmDto filmDto = service.update(FilmTestData.FILM_DTO_1, 1);
        log.info("Testing update(): " + filmDto);
        assertEquals(filmDto, FilmTestData.FILM_DTO_1);
    }

    @Test
    @Order(5)
    @Override
    protected void delete() {
        Mockito.doNothing().when(repository).deleteById(1);
        service.delete(1);
        verify(repository, times(1)).deleteById(eq(1));
    }

    @Test
    @Order(6)
    protected void searchFilmsTest() {
        String title = "title";
        Mockito.when(((FilmRepository) repository).findAllByTitleContainsIgnoreCase(title)).thenReturn(FilmTestData.FILM_LIST);
        Mockito.when(mapper.toDtos(FilmTestData.FILM_LIST)).thenReturn(FilmTestData.FILM_DTO_LIST);
        List<FilmWithDirectorsDto> filmDtoList = ((FilmService) service).searchFilms(title);
        log.info("Testing update(): " + filmDtoList);
        assertEquals(filmDtoList, FilmTestData.FILM_DTO_LIST);
    }
}
