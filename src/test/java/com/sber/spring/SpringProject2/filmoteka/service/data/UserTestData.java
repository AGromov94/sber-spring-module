package com.sber.spring.SpringProject2.filmoteka.service.data;

import com.sber.spring.SpringProject2.filmoteka.dto.RoleDto;
import com.sber.spring.SpringProject2.filmoteka.dto.UserDto;
import com.sber.spring.SpringProject2.filmoteka.model.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface UserTestData {
    Film FILM_1 = new Film("title1",
            Genre.DOCUMENTARY,
            "country1",
            "year1",
            "fileDirectory1",
            new ArrayList<Director>(),
            new ArrayList<Order>());

    Film FILM_2 = new Film("title2",
            Genre.DOCUMENTARY,
            "country2",
            "year2",
            "fileDirectory2",
            new ArrayList<Director>(),
            new ArrayList<Order>());

    Film FILM_3 = new Film("title3",
            Genre.DOCUMENTARY,
            "country3",
            "year3",
            "fileDirectory3",
            new ArrayList<Director>(),
            new ArrayList<Order>());
    List<Integer> ordersIds = Arrays.asList(1, 2, 3);
    Order order1 = new Order(new User(),FILM_1, LocalDate.of(1888,4,4), 2,  LocalDate.of(1890,4,4), true);
    Order order2 = new Order(new User(),FILM_2, LocalDate.of(1888,4,4), 2,  LocalDate.of(1890,4,4), true);
    Order order3 = new Order(new User(),FILM_3, LocalDate.of(1888,4,4), 2,  LocalDate.of(1890,4,4), true);
    List<Order> orders = Arrays.asList(order1, order2, order3);
    UserDto USER_DTO_1 = new UserDto(
            "login1",
            "password1",
            "firstName1",
            "lastName1",
            "middleName1",
            LocalDate.of(1991, 1, 1),
            "phone1",
            "address1",
            "email1",
            new RoleDto(),
            ordersIds,
            "");

    UserDto USER_DTO_2 = new UserDto(
            "login2",
            "password2",
            "firstName2",
            "lastName2",
            "middleName2",
            LocalDate.of(1992, 2, 2),
            "phone2",
            "address2",
            "email2",
            new RoleDto(),
            new ArrayList<Integer>(),
            "");

    UserDto USER_DTO_3 = new UserDto(
            "login3",
            "password3",
            "firstName3",
            "lastName3",
            "middleName3",
            LocalDate.of(1993, 3, 3),
            "phone3",
            "address3",
            "email3",
            new RoleDto(),
            new ArrayList<Integer>(),
            "");

    List<UserDto> USER_DTO_LIST = Arrays.asList(USER_DTO_1, USER_DTO_2, USER_DTO_3);

    User USER_1 = new User(
            "login1",
            "password1",
            "firstName1",
            "lastName1",
            "middleName1",
            LocalDate.of(1991, 1, 1),
            "phone1",
            "address1",
            "email1",
            new Role(1,"title1", "user"),
            orders,
            "");

    User USER_2 = new User(
            "login2",
            "password2",
            "firstName2",
            "lastName2",
            "middleName2",
            LocalDate.of(1992, 2, 2),
            "phone2",
            "address2",
            "email2",
            new Role(),
            new ArrayList<Order>(),
            "");

    User USER_3 = new User(
            "login3",
            "password3",
            "firstName3",
            "lastName3",
            "middleName3",
            LocalDate.of(1993, 3, 3),
            "phone3",
            "address3",
            "email3",
            new Role(),
            new ArrayList<Order>(),
            "");

    List<User> USER_LIST = Arrays.asList(USER_1, USER_2, USER_3);
}
