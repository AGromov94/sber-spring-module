package com.sber.spring.SpringProject2.filmoteka.dto;

import lombok.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto extends GenericDto{
    private Integer UserId;
    private Integer FilmId;
    private LocalDate rentDate;
    private Integer rentPeriod;
    private LocalDate returnDate;
    private Boolean purchase;

}
