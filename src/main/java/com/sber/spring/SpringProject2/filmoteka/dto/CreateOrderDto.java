package com.sber.spring.SpringProject2.filmoteka.dto;

import lombok.Data;
import lombok.ToString;

import java.time.LocalDate;

@Data
@ToString
public class CreateOrderDto {
    private Integer UserId;
    private Integer FilmId;
    private LocalDate rentDate;
    private Integer rentPeriod;
    private LocalDate returnDate;
    private Boolean purchase;
}
