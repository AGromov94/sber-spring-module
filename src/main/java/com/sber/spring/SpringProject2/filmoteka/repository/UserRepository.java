package com.sber.spring.SpringProject2.filmoteka.repository;

import com.sber.spring.SpringProject2.filmoteka.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends GenericRepository<User> {
    @Query(nativeQuery = true,
            value = """
                select *
                from users
                where upper(login) = upper(:login)
                and is_deleted = false
            """)
    User findUserByLoginAndDeletedFalse(final String login);

    @Query(nativeQuery = true,
            value = """
                        select *
                        from users
                        where upper(email) = upper(:email)
                        and is_deleted = false
                    """)
    User findUserByEmailAndDeletedFalse(final String email);

    @Query(nativeQuery = true,
            value = """
                        select *
                        from users
                        where upper(phone) = upper(:phone)
                        and is_deleted = false
                    """)
    User findUserByPhoneAndDeletedFalse(final String phone);

    @Query(nativeQuery = true,
            value = """
                 select u.*
                 from users u
                 where u.first_name ilike '%' || coalesce(:firstName, '%') || '%'
                 and u.last_name ilike '%' || coalesce(:lastName, '%') || '%'
                 and u.login ilike '%' || coalesce(:login, '%') || '%'
                  """)
    Page<User> searchUsers(String firstName,
                           String lastName,
                           String login,
                           Pageable pageable);

//    @Query(nativeQuery = true,
//            value = """
//                 select distinct email
//                 from users u join user_book_rents bri on u.id = bri.user_id
//                 where cast(bri.rent_date as date) + bri.rent_period < now()
//                 and bri.returned = false
//                 and u.is_deleted = false
//                 """)
//    List<String> getDelayedEmails();

    User findUserByChangePasswordToken(String uuid);

}
