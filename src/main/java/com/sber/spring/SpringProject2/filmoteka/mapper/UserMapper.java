package com.sber.spring.SpringProject2.filmoteka.mapper;

import com.sber.spring.SpringProject2.filmoteka.dto.UserDto;
import com.sber.spring.SpringProject2.filmoteka.model.GenericModel;
import com.sber.spring.SpringProject2.filmoteka.model.Order;
import com.sber.spring.SpringProject2.filmoteka.model.User;
import com.sber.spring.SpringProject2.filmoteka.repository.OrderRepository;
import com.sber.spring.SpringProject2.filmoteka.repository.RoleRepository;
import com.sber.spring.SpringProject2.filmoteka.repository.UserRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class UserMapper extends GenericMapperImpl<User, UserDto> {

    private final OrderRepository orderRepository;
    //private final RoleRepository roleRepository;

    public UserMapper(ModelMapper modelMapper, OrderRepository orderRepository, RoleRepository roleRepository) {
        super(User.class, UserDto.class, modelMapper);
        this.orderRepository = orderRepository;
        //this.roleRepository = roleRepository;
    }

    @PostConstruct
    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(User.class, UserDto.class)
                //.addMappings(m -> m.skip(UserDto::setRole)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(UserDto::setOrderIds)).setPostConverter(toDtoConverter());

        modelMapper.createTypeMap(UserDto.class, User.class)
                //.addMappings(m -> m.skip(User::setRole)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(User::setOrders)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(User source, UserDto destination) {
        //destination.setRole(fillRoleId(source));
        destination.setOrderIds(fillIds(source));
    }

    @Override
    protected void mapSpecificFields(UserDto source, User destination) {
//        if (!Objects.isNull(source.getRole())) {
//            destination.setRole(roleRepository.findById(source.getRole()));
//        } else {
//            destination.setRole(null);
//        }
        if (!Objects.isNull(source.getOrderIds())) {
            destination.setOrders(orderRepository.findAllById(source.getOrderIds()));
        } else {
            destination.setOrders(Collections.emptyList());
        }
    }

    @Override
    protected List<Integer> fillIds(User source) {
        return Objects.isNull(source) || Objects.isNull(source.getOrders()) || source.getOrders().isEmpty()
                ? null
                : source.getOrders().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toList());
    }

    protected Integer fillRoleId(User source) {
        return Objects.isNull(source) ? null : source.getRole().getId();
    }
}
