package com.sber.spring.SpringProject2.filmoteka.controller.mvc;

import com.sber.spring.SpringProject2.filmoteka.dto.UserDto;
import com.sber.spring.SpringProject2.filmoteka.exception.MyDeleteException;
import com.sber.spring.SpringProject2.filmoteka.exception.NotFoundException;
import com.sber.spring.SpringProject2.filmoteka.service.UserService;
import com.sber.spring.SpringProject2.filmoteka.service.userdetails.CustomUserDetails;
import jakarta.security.auth.message.AuthException;
import jakarta.websocket.server.PathParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

import static com.sber.spring.SpringProject2.filmoteka.constants.Error.Users.USER_FORBIDDEN_ERROR;

@Controller
@Slf4j
@RequestMapping("/users")
@RequiredArgsConstructor

public class MvcUserController {
    private final UserService userService;
    @Value("${spring.security.user.name}")
    private String adminUser;

    @GetMapping("/registration")
    public String registrationForm(final Model model) {
        model.addAttribute("userForm", new UserDto());
        return "registration";
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute("userForm") UserDto userDto,
                               BindingResult bindingResult) {
        if (userDto.getLogin().equalsIgnoreCase(adminUser) || userService.getUserByLogin(userDto.getLogin()) != null) {
            bindingResult.rejectValue("login", "error.login", "Такой логин уже зарегистрирован");
            return "registration";
        }
        if (userService.getUserByEmail(userDto.getEmail()) != null) {
            bindingResult.rejectValue("email", "error.email", "Такой email уже зарегистрирован");
            return "registration";
        }
        if (userService.getUserByPhone(userDto.getPhone()) != null) {
            bindingResult.rejectValue("phone", "error.phone", "Такой телефон уже зарегистрирован");
            return "registration";
        }
        userService.create(userDto);
        return "redirect:/login";
    }

    @GetMapping("/remember-password")
    public String rememberPassword() {
        return "users/rememberPassword";
    }

    @PostMapping("/remember-password")
    public String rememberPassword(@ModelAttribute("changePasswordForm") UserDto userDTO) {
        userDTO = userService.getUserByEmail(userDTO.getEmail());
        if (Objects.isNull(userDTO)) {
            return "Error!";
        } else {
            userService.sendChangePasswordEmail(userDTO);
            return "redirect:/login";
        }
    }

    @GetMapping("/change-password")
    public String changePassword(@PathParam(value = "uuid") String uuid,
                                 Model model) {
        model.addAttribute("uuid", uuid);
        return "users/changePassword";
    }

    @PostMapping("/change-password")
    public String changePassword(@PathParam(value = "uuid") String uuid,
                                 @ModelAttribute("changePasswordForm") UserDto userDTO) {
        userService.changePassword(uuid, userDTO.getPassword());
        return "redirect:/login";
    }

    @GetMapping("/change-password/user")
    public String changePassword(Model model) {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserDto userDTO = userService.getOne(customUserDetails.getId());
        UUID uuid = UUID.randomUUID();
        userDTO.setChangePasswordToken(uuid.toString());
        userService.update(userDTO, userDTO.getId());
        model.addAttribute("uuid", uuid);
        return "users/changePassword";
    }

    @GetMapping("/profile/{id}")
    public String userProfileForm(@PathVariable Integer id,
                                  Model model) throws NotFoundException, AuthException {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!Objects.isNull(customUserDetails.getId())) {
            if (!adminUser.equalsIgnoreCase(customUserDetails.getUsername())) {
                if (!id.equals(customUserDetails.getId())) {
                    throw new AuthException(HttpStatus.FORBIDDEN + ": " + USER_FORBIDDEN_ERROR);
                }
            }

        }
        model.addAttribute("user", userService.getOne(id));
        return "profile/viewUserProfile";
    }

    @GetMapping("/list")
    public String listAllUsers(@RequestParam(value = "page", defaultValue = "1") int page,
                               @RequestParam(value = "size", defaultValue = "3") int pageSize,
                               Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "login"));
        List<UserDto> userDtoList = userService.listAll();
        model.addAttribute("users", userDtoList);
        return "users/viewAllUsers";
    }

    @GetMapping("/add-librarian")
    public String addLibrarianPage(Model model) {
        model.addAttribute("userForm", new UserDto());
        return "registration";
    }


    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Integer id) throws MyDeleteException {
        userService.deleteSoft(id);
        return "redirect:/users/list";
    }

    @GetMapping("/restore/{id}")
    public String restore(@PathVariable Integer id) {
        userService.restore(id);
        return "redirect:/users/list";
    }
}
