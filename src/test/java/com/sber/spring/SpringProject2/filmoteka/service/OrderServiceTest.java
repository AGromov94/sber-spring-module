package com.sber.spring.SpringProject2.filmoteka.service;

import com.sber.spring.SpringProject2.filmoteka.dto.OrderDto;
import com.sber.spring.SpringProject2.filmoteka.exception.NotFoundException;
import com.sber.spring.SpringProject2.filmoteka.mapper.OrderMapper;
import com.sber.spring.SpringProject2.filmoteka.model.Order;
import com.sber.spring.SpringProject2.filmoteka.repository.OrderRepository;
import com.sber.spring.SpringProject2.filmoteka.service.data.OrderTestData;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@Slf4j
public class OrderServiceTest extends GenericTest<Order, OrderDto> {
    public OrderServiceTest() {
        repository = Mockito.mock(OrderRepository.class);
        mapper = Mockito.mock(OrderMapper.class);
        service = new OrderService((OrderRepository) repository, (OrderMapper) mapper);
    }

    @Test
    @Override
    protected void getAll() {
        Mockito.when(repository.findAll()).thenReturn(OrderTestData.ORDER_LIST);
        Mockito.when(mapper.toDtos(OrderTestData.ORDER_LIST)).thenReturn(OrderTestData.ORDER_DTO_LIST);
        List<OrderDto> orderDtos = service.listAll();
        log.info("Testing getAll(): " + orderDtos.toString());
        assertEquals(orderDtos.size(), OrderTestData.ORDER_LIST.size());
    }

    @Test
    @Override
    protected void getOne() throws NotFoundException {
        Order order1 = OrderTestData.ORDER_1;
        order1.setId(1);
        OrderDto orderDtoInitial1 = OrderTestData.ORDER_DTO_1;
        orderDtoInitial1.setId(1);

        Order order2 = OrderTestData.ORDER_2;
        order2.setId(2);
        OrderDto orderDtoInitial2 = OrderTestData.ORDER_DTO_2;
        orderDtoInitial2.setId(2);

        Mockito.when(repository.findById(1)).thenReturn(Optional.of(order1));
        Mockito.when(repository.findById(2)).thenReturn(Optional.of(order2));
        Mockito.when(mapper.toDto(order1)).thenReturn(orderDtoInitial1);
        Mockito.when(mapper.toDto(order2)).thenReturn(orderDtoInitial2);

        OrderDto orderDto1 = service.getOne(1);
        OrderDto orderDto2 = service.getOne(2);
        log.info("Testing getOne(): " + orderDto1);
        log.info("Testing getOne(): " + orderDto2);
        assertEquals(orderDto1, OrderTestData.ORDER_DTO_1);
        assertNotEquals(orderDto2, OrderTestData.ORDER_DTO_1);
    }

    @Test
    @Override
    protected void create() {
        Mockito.when(mapper.toEntity(OrderTestData.ORDER_DTO_1)).thenReturn(OrderTestData.ORDER_1);
        Mockito.when(mapper.toDto(OrderTestData.ORDER_1)).thenReturn(OrderTestData.ORDER_DTO_1);
        Mockito.when(repository.save(OrderTestData.ORDER_1)).thenReturn(OrderTestData.ORDER_1);
        OrderDto orderDto = service.create(OrderTestData.ORDER_DTO_1);
        log.info("Testing create(): " + orderDto);
        assertEquals(orderDto, OrderTestData.ORDER_DTO_1);
    }

    @Test
    @Override
    protected void update() {
        Mockito.when(mapper.toEntity(OrderTestData.ORDER_DTO_1)).thenReturn(OrderTestData.ORDER_1);
        Mockito.when(mapper.toDto(OrderTestData.ORDER_1)).thenReturn(OrderTestData.ORDER_DTO_1);
        Mockito.when(repository.save(OrderTestData.ORDER_1)).thenReturn(OrderTestData.ORDER_1);
        OrderDto orderDto = service.update(OrderTestData.ORDER_DTO_1, 1);
        log.info("Testing update(): " + orderDto);
        assertEquals(orderDto, OrderTestData.ORDER_DTO_1);
    }

    @Test
    @Override
    protected void delete() {
        Mockito.doNothing().when(repository).deleteById(1);
        service.delete(1);
        verify(repository, times(1)).deleteById(eq(1));
    }
}
