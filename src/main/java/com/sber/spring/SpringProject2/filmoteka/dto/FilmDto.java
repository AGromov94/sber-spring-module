package com.sber.spring.SpringProject2.filmoteka.dto;

import com.sber.spring.SpringProject2.filmoteka.model.Genre;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

//DTO - Data Transfer Object
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FilmDto extends GenericDto {
    private String title;
    private Genre genre;
    private String country;
    private String premierYear;
    private List<Integer> directorIds = new ArrayList<>();
    private List<Integer> orderIds = new ArrayList<>();
}
