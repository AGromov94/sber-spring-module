package com.sber.spring.SpringProject2.filmoteka.dto;

import jdk.dynalink.linker.LinkerServices;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.relational.core.sql.In;

import java.util.List;

@Data
@ToString
public class CreateDirectorDto {
    private String directorsFio;
    private String position;
    private List<Integer> filmIds;
}
