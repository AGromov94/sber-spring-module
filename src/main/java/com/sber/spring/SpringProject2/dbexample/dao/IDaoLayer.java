package com.sber.spring.SpringProject2.dbexample.dao;

import java.sql.SQLException;
import java.util.List;

public interface IDaoLayer <T, ID>{
    T getOneObjectById(ID id) throws SQLException;

    List<T> getAllObjects();

    T create(T newObject);

    T update(T updatedObject, ID id);

    void deleteObjectById(ID id);
}
