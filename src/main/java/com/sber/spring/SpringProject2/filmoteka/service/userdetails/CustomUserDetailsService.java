package com.sber.spring.SpringProject2.filmoteka.service.userdetails;

import com.sber.spring.SpringProject2.filmoteka.model.User;
import com.sber.spring.SpringProject2.filmoteka.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class CustomUserDetailsService implements UserDetailsService {
    @Value("${spring.security.user.name}")
    private String adminUser;
    @Value("${spring.security.user.password}")
    private String adminPassword;
    @Value("${spring.security.user.roles}")
    private String adminRole;
    private final BCryptPasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    public CustomUserDetailsService(BCryptPasswordEncoder passwordEncoder, UserRepository userRepository) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
    }

    /*
    Тут мы ищем пользователя в БД и авторизуем его
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.info(passwordEncoder.encode("admin"));//отобразить пароль в зашифрованном виде в логе
        if (username.equals(adminUser)) {
            log.info("USERNAME: " + username);
            // админ хранится в памяти приложения (inMemory), а не в БД
            return new CustomUserDetails(null, adminUser, adminPassword, List.of(new SimpleGrantedAuthority("ROLE_" + adminRole)));
        } else {
            User user = userRepository.findUserByLoginAndDeletedFalse(username);
            return new CustomUserDetails(user);
        }
    }
}
