package com.sber.spring.SpringProject2.filmoteka.controller.rest;

import com.sber.spring.SpringProject2.filmoteka.dto.CreateDirectorDto;
import com.sber.spring.SpringProject2.filmoteka.dto.CreateOrderDto;
import com.sber.spring.SpringProject2.filmoteka.dto.DirectorDto;
import com.sber.spring.SpringProject2.filmoteka.dto.OrderDto;
import com.sber.spring.SpringProject2.filmoteka.exception.NotFoundException;
import com.sber.spring.SpringProject2.filmoteka.model.Director;
import com.sber.spring.SpringProject2.filmoteka.model.Order;
import com.sber.spring.SpringProject2.filmoteka.repository.OrderRepository;
import com.sber.spring.SpringProject2.filmoteka.service.OrderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/rest/orders")
@Slf4j
@Tag(name = "Заказы", description = "Контроллер для работы с заказами")
@RequiredArgsConstructor
public class OrderController {
    private final OrderService orderService;

    @Operation(description = "Получить список всех заказов")
    @RequestMapping(value = "/getListOfOrders", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    //тип запроса - GET
    public ResponseEntity<List<OrderDto>> getAllOrders() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(orderService.listAll());
    }

    @Operation(description = "Получить заказ по id")
    @RequestMapping(value = "/getOrder", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    //тип запроса - GET
    public ResponseEntity<OrderDto> getOneOrder(@RequestParam(value = "id") Integer id) throws NotFoundException {
        return ResponseEntity.status(HttpStatus.OK)
                .body(orderService.getOne(id));
    }

    @Operation(description = "Добавить новый заказ")
    @RequestMapping(value = "/addOrder", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    //тип запроса - POST
    public ResponseEntity<OrderDto> createOrder(@RequestBody CreateOrderDto newOrder) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(orderService.createOrderByRequest(newOrder));
    }

    @Operation(description = "Обновить заказ по id")
    @RequestMapping(value = "/updateOrder", method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    //тип запроса - PUT
    public ResponseEntity<OrderDto> updateOrder(@RequestBody OrderDto updateOrder, @RequestParam(value = "id") Integer id) {
        updateOrder.setId(id);
        updateOrder.setUpdatedWhen(LocalDateTime.now());
        updateOrder.setUpdatedBy("ADMIN");
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(orderService.update(updateOrder, id));
    }

    @Operation(description = "Удалить заказ")
    @RequestMapping(value = "/deleteOrder/{id}", method = RequestMethod.DELETE)
    //тип запроса - DELETE
    public void deleteOrder(@PathVariable(value = "id") Integer id) {
        orderService.delete(id);
    }
}
