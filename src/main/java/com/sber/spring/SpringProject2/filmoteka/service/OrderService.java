package com.sber.spring.SpringProject2.filmoteka.service;

import com.sber.spring.SpringProject2.filmoteka.dto.CreateOrderDto;
import com.sber.spring.SpringProject2.filmoteka.dto.OrderDto;
import com.sber.spring.SpringProject2.filmoteka.mapper.OrderMapper;
import com.sber.spring.SpringProject2.filmoteka.model.Order;
import com.sber.spring.SpringProject2.filmoteka.repository.OrderRepository;
import org.springframework.stereotype.Service;

@Service
public class OrderService extends GenericService<Order, OrderDto> {

    protected OrderService(OrderRepository repository, OrderMapper mapper) {
        super(repository, mapper);
    }

    public OrderDto createOrderByRequest(CreateOrderDto newOrder) {
        OrderDto orderDto = new OrderDto();
        orderDto.setUserId(newOrder.getUserId());
        orderDto.setFilmId(newOrder.getFilmId());
        orderDto.setRentDate(newOrder.getRentDate());
        orderDto.setRentPeriod(newOrder.getRentPeriod());
        orderDto.setReturnDate(newOrder.getReturnDate());
        orderDto.setPurchase(newOrder.getPurchase());
        return super.create(orderDto);
    }
}
