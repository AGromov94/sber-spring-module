package com.sber.spring.SpringProject2.dbexample.dao.impl;

import com.sber.spring.SpringProject2.dbexample.dao.IDaoLayer;
import com.sber.spring.SpringProject2.dbexample.database.DatabaseConnection;
import com.sber.spring.SpringProject2.dbexample.model.Book;
import lombok.extern.slf4j.Slf4j;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//CRUD операции Create Read Update Delete
//@Slf4j //потокобезопасен
//public class BookDaoJDBCImpl implements IDaoLayer<Book, Integer> {


//    private static final Connection DB_CONNECTION;
//
//    static {
//        try {
//            DB_CONNECTION = DatabaseConnection.INSTANCE.getConnection();
//        } catch (SQLException e) {
//            throw new RuntimeException(e);
//        }
//    }
//
//    @Override
//    public Book getOneObjectById(Integer id) throws SQLException {
//        final String sqlQuery = "Select * from books where id = ?";
//        try {
//            PreparedStatement query = DB_CONNECTION.prepareStatement(sqlQuery);
//            query.setInt(1, id);
//            ResultSet result = query.executeQuery();
//
//            Book book = new Book(); //потому что метод вернет 1 строчку можно создать объект вне цикла while
//            while (result.next()) {
//                book.setId(result.getInt("id"));
//                book.setAuthor(result.getString("author"));
//                book.setTitle(result.getString("title"));
//                book.setDateAdded(result.getDate("date_added"));
//            }
//            return book;
//        } catch (SQLException e) {
//            log.error("Error: " + e.getMessage());
//        }
//        return null;
//    }
//
//    @Override
//    public List<Book> getAllObjects() {
//        try (Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/spring_database",
//                "postgres",
//                "12345")) {
//            if (connection != null) {
//                log.info("Ура, мы подключились к базе данных!");
//            } else {
//                log.info("База не доступна!");
//            }
//            final String sqlQuery = "Select * from books";
//            PreparedStatement query = connection.prepareStatement(sqlQuery);
//            ResultSet result = query.executeQuery();
//            List<Book> books = new ArrayList<>();
//            while (result.next()) {
//                Book book = new Book();
//                book.setId(result.getInt("id"));
//                book.setAuthor(result.getString("author"));
//                book.setTitle(result.getString("title"));
//                book.setDateAdded(result.getDate("date_added"));
//                books.add(book);
//            }
//            return books;
//        } catch (SQLException e) {
//            log.error("Error: " + e.getMessage());
//        }
//        return Collections.emptyList();
//    }
//
//    @Override
//    public Book create(Book newObject) {
//        return null;
//    }
//
//    @Override
//    public Book update(Book updatedObject, Integer integer) {
//        return null;
//    }
//
//    @Override
//    public void deleteObjectById(Integer integer) {
//
//    }
//}
