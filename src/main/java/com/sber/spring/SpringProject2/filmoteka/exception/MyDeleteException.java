package com.sber.spring.SpringProject2.filmoteka.exception;

public class MyDeleteException extends Exception{
    public MyDeleteException(String message) {
        super(message);
    }
}
