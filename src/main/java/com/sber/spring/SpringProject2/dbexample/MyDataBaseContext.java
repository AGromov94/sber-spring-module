package com.sber.spring.SpringProject2.dbexample;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static com.sber.spring.SpringProject2.dbexample.costants.DataBaseConstants.*;

@Configuration
public class MyDataBaseContext {
    @Bean
    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:postgresql://" + DB_HOST + ":" + DB_PORT + "/" + DB_NAME,
                DB_USER,
                DB_PASSWORD);
    }
}
