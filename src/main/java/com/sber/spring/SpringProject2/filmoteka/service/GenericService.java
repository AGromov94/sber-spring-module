package com.sber.spring.SpringProject2.filmoteka.service;

import com.sber.spring.SpringProject2.filmoteka.dto.GenericDto;
import com.sber.spring.SpringProject2.filmoteka.exception.MyDeleteException;
import com.sber.spring.SpringProject2.filmoteka.mapper.GenericMapperImpl;
import com.sber.spring.SpringProject2.filmoteka.model.GenericModel;
import com.sber.spring.SpringProject2.filmoteka.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Абстрактный сервис, который хранит в себе реализацию CRUD операций по умолчанию
 * Если реализация отличается от того, что представлено в этом классе,
 * то она переопределяется в сервисе для конкретной сущности
 *
 * @param <T>
 * @param <N>
 */
@Service
@Slf4j
public abstract class GenericService<T extends GenericModel, N extends GenericDto> {
    protected final GenericRepository<T> repository;
    protected final GenericMapperImpl<T, N> mapper;

    protected GenericService(GenericRepository<T> repository, GenericMapperImpl<T, N> mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public List<N> listAll() {
        return mapper.toDtos(repository.findAll());
    }

    public List<N> listAllNotDeleted() {
        return mapper.toDtos(repository.findAllByIsDeletedFalse());
    }

    public Page<N> listAll(Pageable pageable) {
        Page<T> preResult = repository.findAll(pageable);
        List<N> result = mapper.toDtos(preResult.getContent());
        return new PageImpl<>(result, pageable, preResult.getTotalElements());
    }

    public Page<N> listAllNotDeleted(Pageable pageable) {
        Page<T> preResult = repository.findAllByIsDeletedFalse(pageable);
        List<N> result = mapper.toDtos(preResult.getContent());
        return new PageImpl<>(result, pageable, preResult.getTotalElements());
    }

    public N getOne(final Integer id) {
        return mapper.toDto(repository.findById(id).orElseThrow(() -> new NotFoundException("Нет данных по заданному id")));
    }

    public N create(N newEntity) {
        newEntity.setCreatedWhen(LocalDateTime.now());
        //TODO: сделать авторизацию
        newEntity.setCreatedBy("ADMIN");
        return mapper.toDto(repository.save(mapper.toEntity(newEntity)));
    }

    public N update(N updatedEntity, final Integer id) {
        updatedEntity.setUpdatedWhen(LocalDateTime.now());
        //TODO: сделать авторизацию
        updatedEntity.setUpdatedBy("ADMIN");
        updatedEntity.setId(id);
        return mapper.toDto(repository.save(mapper.toEntity(updatedEntity)));
    }

    public void delete(final Integer id) {
        repository.deleteById(id);
    }

    public void deleteSoft(final Integer id) throws MyDeleteException, NotFoundException {
        T obj = repository.findById(id).orElseThrow(() -> new NotFoundException("Объект не найден"));
        markAsDeleted(obj);
        repository.save(obj);
    }

    public void restore(final Integer id) throws NotFoundException {
        T obj = repository.findById(id).orElseThrow(() -> new NotFoundException("Объект не найден"));
        unMarkAsDeleted(obj);
        repository.save(obj);
    }

    private void markAsDeleted(T obj) {
        obj.setDeleted(true);
        obj.setCreatedWhen(LocalDateTime.now());
        obj.setDeletedBy("ADMIN");
    }

    private void unMarkAsDeleted(T obj) {
        obj.setDeleted(false);
        obj.setCreatedWhen(null);
        obj.setDeletedBy(null);
    }
}
