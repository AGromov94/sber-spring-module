package com.sber.spring.SpringProject2.filmoteka.exception;

public class NotFoundException extends Exception {
    public NotFoundException(final String message) {
        super(message);
    }
}
