package com.sber.spring.SpringProject2.dbexample.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class User {
    private String login;
    private Long id;
    private String password;
    private String fio;
}
