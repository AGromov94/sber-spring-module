package com.sber.spring.SpringProject2.filmoteka.service.data;

import com.sber.spring.SpringProject2.filmoteka.dto.DirectorDto;
import com.sber.spring.SpringProject2.filmoteka.dto.FilmDto;
import com.sber.spring.SpringProject2.filmoteka.model.Director;
import com.sber.spring.SpringProject2.filmoteka.model.Film;
import com.sber.spring.SpringProject2.filmoteka.model.Genre;
import com.sber.spring.SpringProject2.filmoteka.model.Order;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface FilmTestData {
    List<Integer> directorsIds = Arrays.asList(1);
    FilmDto FILM_DTO_1 = new FilmDto("title1",
            Genre.DOCUMENTARY,
            "country1",
            "year1",
            directorsIds,
            new ArrayList<Integer>());

    FilmDto FILM_DTO_2 = new FilmDto("title2",
            Genre.DOCUMENTARY,
            "country2",
            "year2",
            new ArrayList<Integer>(),
            new ArrayList<Integer>());

    FilmDto FILM_DTO_3 = new FilmDto("title3",
            Genre.DOCUMENTARY,
            "country3",
            "year3",
            new ArrayList<Integer>(),
            new ArrayList<Integer>());

    List<FilmDto> FILM_DTO_LIST = Arrays.asList(FILM_DTO_1, FILM_DTO_2, FILM_DTO_3);

    Film FILM_1 = new Film("title1",
            Genre.DOCUMENTARY,
            "country1",
            "year1",
            "fileDirectory1",
            new ArrayList<Director>(),
            new ArrayList<Order>());

    Film FILM_2 = new Film("title2",
            Genre.DOCUMENTARY,
            "country2",
            "year2",
            "fileDirectory2",
            new ArrayList<Director>(),
            new ArrayList<Order>());

    Film FILM_3 = new Film("title3",
            Genre.DOCUMENTARY,
            "country3",
            "year3",
            "fileDirectory3",
            new ArrayList<Director>(),
            new ArrayList<Order>());

    List<Film> FILM_LIST = Arrays.asList(FILM_1, FILM_2, FILM_3);
}
