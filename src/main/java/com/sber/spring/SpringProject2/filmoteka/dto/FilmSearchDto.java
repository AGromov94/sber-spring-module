package com.sber.spring.SpringProject2.filmoteka.dto;

import com.sber.spring.SpringProject2.filmoteka.model.Genre;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class FilmSearchDto extends GenericDto{
    private String title;
    private Genre genre;
}
