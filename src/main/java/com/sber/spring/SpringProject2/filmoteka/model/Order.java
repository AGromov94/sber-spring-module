package com.sber.spring.SpringProject2.filmoteka.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;

@Entity
@Table(name = "orders")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Order extends GenericModel {

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false,
            foreignKey = @ForeignKey(name = "FK_USER_ORDER"))
    private User user;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "film_id", nullable = false,
            foreignKey = @ForeignKey(name = "FK_FILM_ORDER"))
    private Film film;

    @Column(nullable = false)
    private LocalDate rentDate;

    //кол-во дней аренды фильма по умолчанию 14 дней
    @Column(nullable = false)
    private Integer rentPeriod;

    //расчитывается как rentDate + rentPeriod
    @Column(nullable = false)
    private LocalDate returnDate;

    //флаг куплен/не куплен фильм
    @Column(nullable = false, columnDefinition = "boolean default false") //при вставке записи в таблицу поле автоматически принимает значение false
    private Boolean purchase;
}
