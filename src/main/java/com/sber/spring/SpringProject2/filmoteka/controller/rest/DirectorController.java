package com.sber.spring.SpringProject2.filmoteka.controller.rest;

import com.sber.spring.SpringProject2.filmoteka.dto.AddFilmDto;
import com.sber.spring.SpringProject2.filmoteka.dto.CreateDirectorDto;
import com.sber.spring.SpringProject2.filmoteka.dto.DirectorDto;
import com.sber.spring.SpringProject2.filmoteka.exception.NotFoundException;
import com.sber.spring.SpringProject2.filmoteka.service.DirectorService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/directors")
@Slf4j
@Tag(name = "Режиссеры", description = "Контроллер для работы с режиссерами")
@RequiredArgsConstructor
public class DirectorController {
    private final DirectorService directorService;

    @Operation(description = "Получить список всех режиссеров") //swagger openapi настройка
    @RequestMapping(value = "/getListOfDirectors", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    //тип запроса - get
    public ResponseEntity<List<DirectorDto>> getAllDirectors() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(directorService.listAll());
    }

    @Operation(description = "Получить режиссера по id")
    @RequestMapping(value = "/getDirector", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DirectorDto> getOneDirector(@RequestParam(value = "id") Integer id) throws NotFoundException {
        return ResponseEntity.status(HttpStatus.OK)
                .body(directorService.getOne(id));
    }

    @Operation(description = "Добавить нового режиссера")
    @RequestMapping(value = "/addDirector", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DirectorDto> createDirector(@RequestBody CreateDirectorDto newDirector) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(directorService.createDirectorByRequest(newDirector));
    }

    @Operation(description = "Обновить режиссера по id")
    @RequestMapping(value = "/updateDirector", method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DirectorDto> updateDirector(@RequestBody DirectorDto updatedDirector, @RequestParam(value = "id") Integer id) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(directorService.update(updatedDirector, id));
    }

    @Operation(description = "Удалить режиссера")
    @RequestMapping(value = "/deleteDirector/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(value = "id") Integer id) {
        directorService.delete(id);
    }

    @Operation(description = "Добавить фильм к режиссеру")
    @RequestMapping(value = "/addFilm", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DirectorDto> addFilm(@RequestBody AddFilmDto addFilmDto) throws NotFoundException {
        return ResponseEntity.status(HttpStatus.CREATED).body(directorService.addFilm(addFilmDto));
    }

//    @Operation(description = "Получить список всех режиссеров")
//    @RequestMapping(value = "/getListOfDirectors", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//    //тип запроса - GET
//    public ResponseEntity<List<Director>> getAllDirectors() {
//        return ResponseEntity.status(HttpStatus.OK)
//                .body(directorRepository.findAll());
//    }
//
//    @Operation(description = "Получить режиссера по id")
//    @RequestMapping(value = "/getDirector", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//    //тип запроса - GET
//    public ResponseEntity<DirectorDto> getOneDirector(@RequestParam(value = "id") Integer id) throws NotFoundException {
//        Director director = directorRepository.findById(id).orElseThrow(() -> new NotFoundException("Режиссер с id " + id + " не найден"));
//        DirectorDto directorDto = new DirectorDto(director);
//        return ResponseEntity.status(HttpStatus.OK)
//                .body(directorDto);
//    }
//
//    @Operation(description = "Добавить нового режиссера")
//    @RequestMapping(value = "/addDirector", method = RequestMethod.POST,
//            produces = MediaType.APPLICATION_JSON_VALUE,
//            consumes = MediaType.APPLICATION_JSON_VALUE)
//    //тип запроса - POST
//    public ResponseEntity<Director> createFilm(@RequestBody Director newDirector) {
//        newDirector.setCreatedWhen(LocalDateTime.now());
//        // TODO: сделать авторизацию
//        newDirector.setCreatedBy("ADMIN");
//        return ResponseEntity.status(HttpStatus.CREATED)
//                .body(directorRepository.save(newDirector));
//    }
//
//    @Operation(description = "Обновить режиссера по id")
//    @RequestMapping(value = "/updateDirector", method = RequestMethod.PUT,
//            produces = MediaType.APPLICATION_JSON_VALUE,
//            consumes = MediaType.APPLICATION_JSON_VALUE)
//    //тип запроса - PUT
//    public ResponseEntity<Director> updateDirector(@RequestBody Director updatedDirector, @RequestParam(value = "id") Integer id) {
//        updatedDirector.setId(id);
//        updatedDirector.setUpdatedWhen(LocalDateTime.now());
//        updatedDirector.setUpdatedBy("ADMIN");
//        return ResponseEntity.status(HttpStatus.CREATED)
//                .body(directorRepository.save(updatedDirector));
//    }
//
//    @Operation(description = "Удалить режиссера")
//    @RequestMapping(value = "/deleteDirector/{id}", method = RequestMethod.DELETE)
//    //тип запроса - DELETE
//    public void delete(@PathVariable(value = "id") Integer id) {
//        directorRepository.deleteById(id);
//    }
}
