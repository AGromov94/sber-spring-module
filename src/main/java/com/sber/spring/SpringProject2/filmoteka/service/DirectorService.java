package com.sber.spring.SpringProject2.filmoteka.service;

import com.sber.spring.SpringProject2.filmoteka.dto.AddFilmDto;
import com.sber.spring.SpringProject2.filmoteka.dto.CreateDirectorDto;
import com.sber.spring.SpringProject2.filmoteka.dto.DirectorDto;
import com.sber.spring.SpringProject2.filmoteka.exception.NotFoundException;
import com.sber.spring.SpringProject2.filmoteka.mapper.DirectorMapper;
import com.sber.spring.SpringProject2.filmoteka.model.Director;
import com.sber.spring.SpringProject2.filmoteka.repository.DirectorRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DirectorService extends GenericService<Director, DirectorDto> {

    public DirectorService(DirectorRepository repository, DirectorMapper mapper) {
        super(repository, mapper);
    }

    public DirectorDto createDirectorByRequest(final CreateDirectorDto createDirectorDto) {
        DirectorDto directorDto = new DirectorDto();
        directorDto.setDirectorsFio(createDirectorDto.getDirectorsFio());
        directorDto.setPosition(createDirectorDto.getPosition());
        directorDto.setFilmIds(createDirectorDto.getFilmIds());
        return super.create(directorDto);
    }

    public DirectorDto addFilm(final AddFilmDto addFilmDto) throws NotFoundException {
        DirectorDto directorDto = getOne(addFilmDto.getDirectorId());
        directorDto.getFilmIds().add(addFilmDto.getFilmId());
        update(directorDto, addFilmDto.getDirectorId());
        return directorDto;
    }

    public List<DirectorDto> searchDirectors(final String fio) {
        List<Director> foundDirectors = ((DirectorRepository) repository).findAllByDirectorsFioContainsIgnoreCaseAndIsDeletedFalse(fio);
        return mapper.toDtos(foundDirectors);
    }
}
