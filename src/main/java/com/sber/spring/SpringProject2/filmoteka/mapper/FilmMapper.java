package com.sber.spring.SpringProject2.filmoteka.mapper;

import com.sber.spring.SpringProject2.filmoteka.dto.FilmDto;
import com.sber.spring.SpringProject2.filmoteka.model.Film;
import com.sber.spring.SpringProject2.filmoteka.model.GenericModel;
import com.sber.spring.SpringProject2.filmoteka.repository.DirectorRepository;
import com.sber.spring.SpringProject2.filmoteka.repository.OrderRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class FilmMapper extends GenericMapperImpl<Film, FilmDto> {
    private final DirectorRepository directorRepository;
    private final OrderRepository orderRepository;

    public FilmMapper(ModelMapper modelMapper, DirectorRepository directorRepository, OrderRepository orderRepository) {
        super(Film.class, FilmDto.class, modelMapper);
        this.directorRepository = directorRepository;
        this.orderRepository = orderRepository;
    }

    @PostConstruct
    @Override
    protected void setupMapper() {
        //создаем новый тип маппинга
        //ничего не делаем с полем directorsIds у FilmDto по умолчанию
        //вместо этого используем Converter, который реализует нашу собственну логику заполнения списка ID режиссеров
        modelMapper.createTypeMap(Film.class, FilmDto.class)
                .addMappings(m -> m.skip(FilmDto::setDirectorIds)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(FilmDto::setOrderIds)).setPostConverter(toDtoConverter());
    }

    /**
     * В данном методе описывается логика заполнения специальных полей, которые есть у DTO, но отсутствуют в Entity
     * В нашем случае мы достаем у Film его режиссеров и  преобразуем List<Director> в List<Integer> с ID режиссеров
     *
     * @param source
     * @param destination
     */
    @Override
    protected void mapSpecificFields(Film source, FilmDto destination) {
        destination.setDirectorIds(fillIds(source));
        destination.setOrderIds(fillOrdersIds(source));
    }

    @Override
    protected void mapSpecificFields(FilmDto source, Film destination) {

    }

    @Override
    protected List<Integer> fillIds(Film source) {
        return Objects.isNull(source) || Objects.isNull(source.getDirectors()) || source.getDirectors().isEmpty()
                ? null
                : source.getDirectors().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toList());
    }

    private List<Integer> fillOrdersIds(Film source) {
        return Objects.isNull(source) || Objects.isNull(source.getOrders()) || source.getOrders().isEmpty()
                ? null
                : source.getOrders().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toList());
    }
}
