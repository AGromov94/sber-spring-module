package com.sber.spring.SpringProject2.filmoteka.model;

import lombok.Getter;

@Getter
public enum Genre {
    FANTASY("Фантастика"),
    THRILLER("Триллер"),
    DRAMA("Драма"),
    DOCUMENTARY("Документальный"),
    HORROR("Ужасы"),
    MILITARY("Военный"),
    CRIMINAL("Криминал");

    private final String genreTextDisplay;

    Genre(String text) {
        this.genreTextDisplay = text;
    }
}
