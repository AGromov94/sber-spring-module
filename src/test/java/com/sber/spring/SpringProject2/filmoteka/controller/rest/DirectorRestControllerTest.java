package com.sber.spring.SpringProject2.filmoteka.controller.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sber.spring.SpringProject2.filmoteka.dto.DirectorDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//В этом классе содержатся интеграционные тесты
@Slf4j
@RequiredArgsConstructor
public class DirectorRestControllerTest extends GenericControllerTest {

    @Test
    @Order(3)
    @Override
    protected void createObject() throws Exception {
        DirectorDto addedDirector = new DirectorDto("Г. Ричи", "Лучший режиссер криминальных драм", new ArrayList<Integer>() {
        });
        log.info("Testing createObject directors via REST started");
        String result = mvc.perform(
                        MockMvcRequestBuilders.post("/rest/directors/addDirector")
                                .content(super.asJsonString(addedDirector))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful()) //проверяем что статус ответа 200
                .andExpect(jsonPath("$.*", hasSize(greaterThan(0)))) //проверяем размер полученного JSONа
                .andReturn()
                .getResponse()
                .getContentAsString();
        assertEquals("Г. Ричи", objectMapper.readValue(result, new TypeReference<DirectorDto>() {
        }).getDirectorsFio());
        log.info(addedDirector.toString());
        log.info("Testing createObject directors via REST finished");
    }

    @Test
    @Order(4)
    @Override
    protected void updateObject() throws Exception {
        DirectorDto updatedDirector = new DirectorDto("Д. Лукас", "Режиссер оригинальной трилогии StarWars", new ArrayList<Integer>() {
        });
        log.info("Testing updateObject directors via REST started");
        String result = mvc.perform(
                        MockMvcRequestBuilders.put("/rest/directors/updateDirector")
                                .content(super.asJsonString(updatedDirector))
                                .param("id", "46")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful()) //проверяем что статус ответа 200
                .andExpect(jsonPath("$.*", hasSize(greaterThan(0)))) //проверяем размер полученного JSONа
                .andReturn()
                .getResponse()
                .getContentAsString();
        assertEquals("Д. Лукас", objectMapper.readValue(result, new TypeReference<DirectorDto>() {
        }).getDirectorsFio());
        log.info(objectMapper.readValue(result, new TypeReference<DirectorDto>() {
        }).toString());
        log.info("Testing updateObject directors via REST finished");
    }

    @Test
    @Order(5)
    @Override
    protected void deleteObject() throws Exception {
        log.info("Testing deleteObject directors via REST started");
        String result1 = mvc.perform(
                        MockMvcRequestBuilders.get("/rest/directors/getListOfDirectors")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse()
                .getContentAsString();
        super.mvc.perform(MockMvcRequestBuilders
                        .delete("/rest/directors/deleteDirector/46")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        String result2 = mvc.perform(
                        MockMvcRequestBuilders.get("/rest/directors/getListOfDirectors")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse()
                .getContentAsString();
        assertFalse(result1.length() == result2.length()); //до удаления != после удаления
        log.info("Testing deleteObject directors via REST finished");
    }

    @Test
    @Order(1)
    @Override
    protected void listAll() throws Exception {
        log.info("Testing listAll directors via REST started");
        String result = mvc.perform(
                        MockMvcRequestBuilders.get("/rest/directors/getListOfDirectors")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful()) //проверяем что статус ответа 200
                .andExpect(jsonPath("$.*", hasSize(greaterThan(0)))) //проверяем размер полученного JSONа
                .andReturn()
                .getResponse()
                .getContentAsString();
        List<DirectorDto> directorDtos = objectMapper.readValue(result, new TypeReference<List<DirectorDto>>() {
        });
        assertFalse(directorDtos.isEmpty()); //проверяем что список режиссеров не пустой
        for (DirectorDto d : directorDtos) {
            log.info("Director: " + d.toString());
        }
        log.info("Testing listAll directors via REST finished");
    }

    @Test
    @Order(2)
    @Override
    protected void listOne() throws Exception {
        log.info("Testing listOne directors via REST started");
        String result = mvc.perform(
                        MockMvcRequestBuilders.get("/rest/directors/getDirector")
                                .param("id", "40")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful()) //проверяем что статус ответа 200
                .andExpect(jsonPath("$.*", hasSize(greaterThan(0)))) //проверяем размер полученного JSONа
                .andReturn()
                .getResponse()
                .getContentAsString();
        DirectorDto directorDto = objectMapper.readValue(result, new TypeReference<DirectorDto>() {
        });
        assertEquals("Г. Ричи", directorDto.getDirectorsFio());
        log.info(directorDto.toString());
        log.info("Testing listOne directors via REST finished");
    }
}
